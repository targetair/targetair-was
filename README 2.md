# 프로젝트 수행
> 로컬 테스트 수행 시에만 해당합니다.
>
> 실제 컨테이너로 배포된 어플리케이션의 경우 secret에 기재된 DB_USER, DB_PASSWORD 를 사용합니다.

- git clone
- npm install
- .env 파일 수정
- npm run dev

## DB 설정 방법

- .env 파일 내 DB_TYPE 값을 변경하고 선택한 DB 에 맞는 속성을 수정합니다.
- 2020.05.18 현재 mongodb, postgresql 만 지원하고 있습니다. (sqlite는 사내망 설치 이슈로 지원하지 않습니다.)

``` yaml
# sqlite / mongodb / postgresql
DB_TYPE=postgresql

# (프로젝트 설정에 따라 아래 정보도 수정 가능합니다.)
DB_USER=helm_user
DB_PASSWORD=new1234!
```

## 프로젝트 정상 확인

http://localhost:3000/ 접속 시 아래와 같이 json 응답이 오면 정상

``` json
{
  "message": "Welcome to express + variety of DB sample."
}
```

## env 값 확인 페이지

http://localhost:3000/dotenv 접속 시 env 목록 중 아래 항목을 확인

``` json
{
  ...
  "DB_TYPE": "postgresql",
  ...
}
```