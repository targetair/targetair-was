/* eslint-disable no-case-declarations */
/* eslint-disable no-unused-vars */
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const cors = require("cors");
const logger = require("morgan");
const fileUpload = require("express-fileupload");
const _ = require("lodash");

require("dotenv").config();

const CONST_DB_TYPE = process.env.DB_TYPE.toUpperCase() || "SQLITE";
console.log("env.DB_TYPE =>", CONST_DB_TYPE);

const app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

// cors options
// const corsOptions = {
//   origin: "http://localhost:8081"
// }

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use(fileUpload({
  createParentPath: true
}));

// import default route
const indexRouter = require("./routes/index");

app.use("/", indexRouter);

const nhRpaOcrRouter = require("./routes/nhRpaOcrRouter");

app.use("/apis/ocr/", nhRpaOcrRouter);

// const employeesPostgresqlRouter = require("./routes/employeesPostgresql");
// app.use("/employee/", employeesPostgresqlRouter);

// env 출력
app.use("/dotenv", (req, res, next) => {
  res.send(process.env);
});

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
