/* eslint-disable no-nested-ternary */
const express = require("express");

const router = express.Router();

// require("../js/rpa-ocr/rpaImageService.js");
const rpaDbServerice = require("../js/rpa-ocr/rpaDbService.js");

// const rpaPdfServerice = require("../js/rpa-ocr/rpaPdfService.js");

const response = {
  responseSet(res, returnCode, returnBody, errorObj) {
    const resultObj = {
      returnCode: "",
      message: "",
      data: null,
    };

    // console.log("returnCode : " + returnCode);
    // console.log("errorObj : ");
    // console.error(errorObj.message);
    if (returnCode != "OK" && errorObj) {
      console.error(errorObj.message);
    }

    resultObj.returnCode = returnCode;
    resultObj.data = returnBody;
    resultObj.errorMsg = errorObj ? errorObj.message : "";
    const httpStatusCode = returnCode == "OK" ? "200" : (errorObj && errorObj.status) ? errorObj.status : 500;
    res.status(httpStatusCode).json(resultObj);
  }
};

// ocr 요청 리스트 조회 (getOcrRequestList)
router.get("/getOcrRequestList", async (req, res) => {
  try {
    // console.log("req", req);

    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    // console.log("reqItem", reqItem);
    const searchParam = {
      itemsPerPage: reqItem && reqItem.itemsPerPage ? parseInt(reqItem.itemsPerPage, 10) || 10 : 10,
      currentPage: reqItem && reqItem.currentPage ? parseInt(reqItem.currentPage, 10) || 1 : 1,
      searchText: reqItem && reqItem.searchText ? reqItem.searchText || "" : "",
      searchStatus: reqItem && reqItem.searchStatus ? reqItem.searchStatus || "" : "",
      userId: reqItem && reqItem.userId ? reqItem.userId || "" : ""
    };

    // 권한체크 필요

    const totalCount = await rpaDbServerice.getOcrRequestTotalCount(searchParam);
    const recordset = await rpaDbServerice.getOcrRequestList(searchParam);

    const returnData = searchParam;
    returnData.totalCount = totalCount;
    returnData.list = recordset.recordset;
    // console.log("list", returnData.list);
    // returnData.list.map((m) => console.log(`aaaaa => ${m.ocrRqstId}`));
    response.responseSet(res, "OK", returnData);
  } catch (err) {
    // console.error(err);
    response.responseSet(res, "NG", null, err);
  }
});

// ocr 요청 상세조회 (getOcrRequestDetail)
router.get("/getOcrRequestDetail", async (req, res) => {
  try {
    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    if (!reqItem || !reqItem.ocrRqstId) {
      const e = new Error("입력값이 유효하지 않습니다.");
      e.status = 400;
      throw e;
    }

    const record = await rpaDbServerice.getOcrRequestDetail(reqItem.ocrRqstId);
    // console.dir(recordset);
    response.responseSet(res, "OK", record);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// ocr 요청 결과조회 (getOcrRequestResult)
router.get("/getOcrRequestResult", async (req, res) => {
  try {
    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    if (!reqItem || !reqItem.ocrRqstId) {
      const e = new Error("입력값이 유효하지 않습니다.");
      e.status = 400;
      throw e;
    }

    // Request 상세조회
    const recordRequest = await rpaDbServerice.getOcrRequestDetail(reqItem.ocrRqstId);
    // Template 상세조회
    const recordTemplate = await rpaDbServerice.getOcrTemplateDetail(recordRequest.ocrTmplId);
    // template image 불필요 삭제
    recordTemplate.tmplImgData = null;
    const resultSet = {
      tmplate: recordTemplate,
      result: recordRequest
    };
    // console.dir(recordset);
    response.responseSet(res, "OK", resultSet);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// ocr 신규 요청 (createOcrRequest)
router.post("/createOcrRequest", async (req, res) => {
  try {
    const reqItem = req.body;
    const reqFiles = req.files;
    console.log("reqItem", reqItem);

    if (!reqFiles || !reqFiles.uploadFile) {
      const e = new Error("File이 첨부되지 않았습니다.");
      e.status = 400;
      throw e;
    }

    if (reqFiles.uploadFile.size > 10 * 1024 * 1024) {
      const e = new Error("File size가 너무 큽니다.");
      e.status = 400;
      throw e;
    }

    reqItem.retryCnt = 0;
    reqItem.ocrStat = "01";
    reqItem.imgOrgData = reqFiles.uploadFile.data.toString("base64");

    console.log(reqItem.imgOrgData.length);

    await rpaDbServerice.createOcrRequest(reqItem);

    const resBody = {
      fileName: reqFiles.uploadFile.name,
      type: reqFiles.uploadFile.mimetype,
      size: reqFiles.uploadFile.size
    };

    response.responseSet(res, "OK", resBody);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// OCR 처리결과 수정 (updateOcrRequestResult)
router.post("/updateOcrRequestResult", async (req, res) => {
  try {
    const reqItem = req.body;

    if (!reqItem || !reqItem.ocrRqstId || !reqItem.ocrRsltModiItem) {
      const e = new Error("입력값이 유효하지 않습니다.");
      e.status = 400;
      throw e;
    }

    // check if exists
    await rpaDbServerice.getOcrRequestDetail(reqItem.ocrRqstId);

    await rpaDbServerice.updateOcrRequest(reqItem);

    response.responseSet(res, "OK");
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 요청 내역 상태 변경 (updateOcrRequestStatus)
router.post("/updateOcrRequestStatus", async (req, res) => {
  try {
    const reqItem = req.body;

    if (!reqItem || !reqItem.ocrRqstId || !reqItem.ocrStat) {
      const e = new Error("입력값이 유효하지 않습니다.");
      e.status = 400;
      throw e;
    }

    // check if exists
    await rpaDbServerice.getOcrRequestDetail(reqItem.ocrRqstId);

    await rpaDbServerice.updateOcrRequest(reqItem);

    response.responseSet(res, "OK");
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// ocr 요청 현황 조회 (getOcrRequestStatistics)
router.get("/getOcrRequestStatistics", async (req, res) => {
  try {
    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    // console.log("reqItem", reqItem);
    const searchParam = {

      userId: reqItem && reqItem.userId ? reqItem.userId || "" : ""
    };

    const recordset = await rpaDbServerice.getOcrRequestStatistics(searchParam);
    // console.dir(recordset);
    // console.dir(recordset.recordset);
    response.responseSet(res, "OK", recordset.recordset);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// OCR 사용가능 Template 리스트 조회 (getOcrTemplateAvailableList)
router.get("/getOcrTemplateAvailableList", async (req, res) => {
  try {
    const recordset = await rpaDbServerice.getOcrTemplateAvailableList();
    response.responseSet(res, "OK", recordset.recordset);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// OCR Template 리스트 조회 (getOcrTemplateList)
router.get("/getOcrTemplateList", async (req, res) => {
  try {
    const recordset = await rpaDbServerice.getOcrTemplateList();
    response.responseSet(res, "OK", recordset.recordset);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// OCR Template 상세조회 (getOcrTemplateDetail)
router.get("/getOcrTemplateDetail", async (req, res) => {
  try {
    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    if (!reqItem || !reqItem.ocrTmplId) {
      const e = new Error("입력값이 유효하지 않습니다.");
      e.status = 400;
      throw e;
    }

    const record = await rpaDbServerice.getOcrTemplateDetail(reqItem.ocrTmplId);
    // console.dir(recordset);
    response.responseSet(res, "OK", record);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 등록 (createOcrTemplate)
router.post("/createOcrTemplate", async (req, res) => {
  try {
    const reqItem = req.body;
    const reqFiles = req.files;

    if (!reqFiles || !reqFiles.uploadFile) {
      const e = new Error("File이 첨부되지 않았습니다.");
      e.status = 400;
      throw e;
    }

    if (reqFiles.uploadFile.size > 3 * 1024 * 1024) {
      const e = new Error("File size가 너무 큽니다.");
      e.status = 400;
      throw e;
    }

    reqItem.tmplUseStat = "01";
    console.log("reqItem", reqItem);

    reqItem.tmplImgData = reqFiles.uploadFile.data.toString("base64");

    await rpaDbServerice.createOcrTemplate(reqItem);

    const resBody = {
      fileName: reqFiles.uploadFile.name,
      type: reqFiles.uploadFile.mimetype,
      size: reqFiles.uploadFile.size
    };

    response.responseSet(res, "OK", resBody);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 변경 (updateOcrTemplate)
router.post("/updateOcrTemplate", async (req, res) => {
  try {
    const reqItem = req.body;
    const reqFiles = req.files;
    console.log("reqItem", reqItem);

    if (!reqFiles || !reqFiles.uploadFile) {
      reqItem.tmplImgData = null;
    } else {
      if (reqFiles.uploadFile.size > 3 * 1024 * 1024) {
        const e = new Error("File size가 너무 큽니다.");
        e.status = 400;
        throw e;
      }
      reqItem.tmplImgData = reqFiles.uploadFile.data.toString("base64");
      console.log(`Upload file size = ${reqItem.tmplImgData.length}`);
    }

    // check if exists
    await rpaDbServerice.getOcrTemplateDetail(reqItem.ocrTmplId);

    await rpaDbServerice.updateOcrTemplate(reqItem);

    response.responseSet(res, "OK");
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 상태 변경 (updateOcrTemplateStatus)
router.post("/updateOcrTemplateStatus", async (req, res) => {
  try {
    const reqItem = req.body;

    console.log(reqItem);

    // check if exists
    const record = await rpaDbServerice.getOcrTemplateDetail(reqItem.ocrTmplId);

    if (record.tmplUseStat === reqItem.tmplUseStat) {
      const e = new Error("상태가 이미 반영되어 있습니다.");
      e.status = 400;
      throw e;
    }

    await rpaDbServerice.updateOcrTemplateStatus(reqItem);

    response.responseSet(res, "OK");
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 제안 리스트 조회 (getTmplProposalList)
router.get("/getTmplProposalList", async (req, res) => {
  try {
    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    console.log("reqItem", reqItem);
    const searchParam = {
      itemsPerPage: reqItem && reqItem.itemsPerPage ? parseInt(reqItem.itemsPerPage, 10) || 10 : 10,
      currentPage: reqItem && reqItem.currentPage ? parseInt(reqItem.currentPage, 10) || 1 : 1,
      searchText: reqItem && reqItem.searchText ? reqItem.searchText || "" : "",
      searchStatus: reqItem && reqItem.searchStatus ? reqItem.searchStatus || "" : "",
      userId: reqItem && reqItem.userId ? reqItem.userId || "" : ""
    };

    const totalCount = await rpaDbServerice.getTmplProposalTotalCount(searchParam);

    const recordset = await rpaDbServerice.getTmplProposalList(searchParam);

    const returnData = searchParam;
    returnData.totalCount = totalCount;
    returnData.list = recordset.recordset;
    // returnData.list.map((m) => console.log(`aaaaa => ${m.ocrSgstId}`));
    response.responseSet(res, "OK", returnData);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// OCR Template 제안 상세조회 (getTmplProposalDetail)
router.get("/getTmplProposalDetail", async (req, res) => {
  try {
    const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
    if (!reqItem || !reqItem.ocrSgstId) {
      const e = new Error("입력값이 유효하지 않습니다.");
      e.status = 400;
      throw e;
    }

    const record = await rpaDbServerice.getTmplProposalDetail(reqItem.ocrSgstId);
    // console.dir(recordset);
    response.responseSet(res, "OK", record);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 신규제안 등록 (createTemplateProposal)
router.post("/createTemplateProposal", async (req, res) => {
  try {
    const reqItem = req.body;
    const reqFiles = req.files;

    if (!reqFiles || !reqFiles.uploadFile) {
      const e = new Error("File이 첨부되지 않았습니다.");
      e.status = 400;
      throw e;
    }

    if (reqFiles.uploadFile.size > 3 * 1024 * 1024) {
      const e = new Error("File size가 너무 큽니다.");
      e.status = 400;
      throw e;
    }

    reqItem.retryCnt = 0;
    reqItem.sgstStat = "01";
    reqItem.imgOrgData = reqFiles.uploadFile.data.toString("base64");

    console.log(`Upload file size = ${reqItem.imgOrgData.length}`);

    await rpaDbServerice.createTemplateProposal(reqItem);

    const resBody = {
      fileName: reqFiles.uploadFile.name,
      type: reqFiles.uploadFile.mimetype,
      size: reqFiles.uploadFile.size
    };

    response.responseSet(res, "OK", resBody);
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 제안 변경 (updateTemplateProposal)
router.post("/updateTemplateProposal", async (req, res) => {
  try {
    const reqItem = req.body;
    const reqFiles = req.files;

    if (!reqFiles || !reqFiles.uploadFile) {
      reqItem.imgOrgData = null;
    } else {
      if (reqFiles.uploadFile.size > 3 * 1024 * 1024) {
        const e = new Error("File size가 너무 큽니다.");
        e.status = 400;
        throw e;
      }
      reqItem.imgOrgData = reqFiles.uploadFile.data.toString("base64");
      console.log(`Upload file size = ${reqItem.imgOrgData.length}`);
    }

    // check if exists
    await rpaDbServerice.getTmplProposalDetail(reqItem.ocrSgstId);

    await rpaDbServerice.updateTemplateProposal(reqItem);

    response.responseSet(res, "OK");
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

// 템플릿 제안 상태 변경 (updateTemplateProposalStatus)
router.post("/updateTemplateProposalStatus", async (req, res) => {
  try {
    const reqItem = req.body;

    // check if exists
    const record = await rpaDbServerice.getTmplProposalDetail(reqItem.ocrSgstId);

    if (record.sgstStat === reqItem.sgstStat) {
      const e = new Error("상태가 이미 반영되어 있습니다.");
      e.status = 400;
      throw e;
    }

    await rpaDbServerice.updateTemplateProposalStatus(reqItem);

    response.responseSet(res, "OK");
  } catch (err) {
    response.responseSet(res, "NG", null, err);
  }
});

module.exports = router;
