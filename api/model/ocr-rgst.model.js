const Sequelize = require("sequelize");

const Model = Sequelize.Model;

module.exports = (sequelize) => {
  class OcrRqst extends Model {}
  return OcrRqst.init(
    {
      // primary key
      ocrRqstUid: {
        type: Sequelize.STRING(32),
        allowNull: false,
        primaryKey: true,
        field: "OCR_RQST_UUID"
      },
      // control fields
      userUid: {
        type: Sequelize.STRING(32),
        allowNull: false,
        primaryKey: false,
        field: "PTL_USR_UUID"
      },
      userNm: {
        type: Sequelize.STRING(20),
        allowNull: false,
        primaryKey: false,
        field: "USRNM"
      },
      ariBrc: {
        type: Sequelize.STRING(9),
        allowNull: true,
        primaryKey: false,
        field: "ARI_BRC"
      },
      ariBrnm: {
        type: Sequelize.STRING(120),
        allowNull: true,
        primaryKey: false,
        field: "ARI_BRNM"
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "RQST_RG_DTM"
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "RQST_MOD_DTM"
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "DEL_DTM"
      },
      delYn: {
        type: Sequelize.STRING(1),
        allowNull: true,
        primaryKey: false,
        field: "DEL_YN"
      },
      // biz fields
      tmplUid: {
        type: Sequelize.STRING(32),
        allowNull: true,
        primaryKey: false,
        field: "TMPL_UUID"
      },
      pupsUse: {
        type: Sequelize.STRING(256),
        allowNull: true,
        primaryKey: false,
        field: "PUPS_USE"
      },
      imgFileName: {
        type: Sequelize.STRING(256),
        allowNull: true,
        primaryKey: false,
        field: "IMG_FILE_NAME"
      },
      imgFileType: {
        type: Sequelize.STRING(120),
        allowNull: true,
        primaryKey: false,
        field: "IMG_FILE_TYPE"
      },
      imgPageCnt: {
        type: Sequelize.INTEGER(),
        allowNull: true,
        primaryKey: false,
        field: "IMG_PAGE_CNT"
      },
      imgOrgData: {
        type: Sequelize.TEXT,
        allowNull: true,
        primaryKey: false,
        field: "IMG_ORG_DATA"
      },
      ocrStat: {
        type: Sequelize.STRING(2),
        allowNull: true,
        primaryKey: false,
        field: "OCR_STAT"
      },
      cmplAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "CMPL_DTM"
      },
      ocrRsltOrigItem: {
        type: Sequelize.TEXT,
        allowNull: true,
        primaryKey: false,
        field: "OCR_RSLT_ORIG_ITEM"
      },
      ocrRsltModItem: {
        type: Sequelize.TEXT,
        allowNull: true,
        primaryKey: false,
        field: "OCR_RSLT_MOD_ITEM"
      },
      retryCnt: {
        type: Sequelize.INTEGER,
        allowNull: true,
        primaryKey: false,
        field: "RETRY_CNT"
      },
    },
    {
      sequelize,
      modelName: "ocrRqst",
      tableName: "TB_RA_PT_OR_OCRRQST",
      freezetableName: true,
      timestamps: true
    }

  );
};
