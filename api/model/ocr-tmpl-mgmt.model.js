const Sequelize = require("sequelize");

const Model = Sequelize.Model;

module.exports = (sequelize) => {
  class OcrTmplMgmt extends Model {}
  return OcrTmplMgmt.init(
    {
      // primary key
      tmplUid: {
        type: Sequelize.STRING(32),
        allowNull: false,
        primaryKey: true,
        field: "TMPL_UUID"
      },
      // control fields
      userUid: {
        type: Sequelize.STRING(32),
        allowNull: false,
        primaryKey: false,
        field: "PTL_USR_UUID"
      },
      userNm: {
        type: Sequelize.STRING(20),
        allowNull: false,
        primaryKey: false,
        field: "USRNM"
      },
      ariBrc: {
        type: Sequelize.STRING(9),
        allowNull: true,
        primaryKey: false,
        field: "ARI_BRC"
      },
      ariBrnm: {
        type: Sequelize.STRING(120),
        allowNull: true,
        primaryKey: false,
        field: "ARI_BRNM"
      },
      createdAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "RQST_RG_DTM"
      },
      updatedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "RQST_MOD_DTM"
      },
      deletedAt: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "DEL_DTM"
      },
      delYn: {
        type: Sequelize.STRING(1),
        allowNull: true,
        primaryKey: false,
        field: "DEL_YN"
      },
      // biz fields
      tmplId: {
        type: Sequelize.STRING(120),
        allowNull: true,
        primaryKey: false,
        field: "TMPLID"
      },
      tmplNm: {
        type: Sequelize.STRING(120),
        allowNull: true,
        primaryKey: false,
        field: "TMPLNM"
      },
      tmplDesc: {
        type: Sequelize.STRING(256),
        allowNull: true,
        primaryKey: false,
        field: "TMPL_DESC"
      },
      tmplStat: {
        type: Sequelize.STRING(2),
        allowNull: true,
        primaryKey: false,
        field: "TMPL_STAT"
      },
      tmplOcrItemCnt: {
        type: Sequelize.INTEGER,
        allowNull: true,
        primaryKey: false,
        field: "TMPL_OCR_ITEM_CNT"
      },
      tmplOcrItem: {
        type: Sequelize.STRING(600),
        allowNull: true,
        primaryKey: false,
        field: "TMPL_OCR_ITEM"
      },
      imgFileType: {
        type: Sequelize.STRING(120),
        allowNull: true,
        primaryKey: false,
        field: "IMG_FILE_TYPE"
      },
      imgOrgData: {
        type: Sequelize.TEXT,
        allowNull: true,
        primaryKey: false,
        field: "IMG_ORG_DATA"
      },
      tmplEfctDtm: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "TMPL_EFCT_DTM"
      },
      tmplExprDtm: {
        type: Sequelize.DATE,
        allowNull: true,
        primaryKey: false,
        field: "TMPL_EXPR_DTM"
      },
      ocrApiUrl: {
        type: Sequelize.STRING(600),
        allowNull: true,
        primaryKey: false,
        field: "OCR_API_URL"
      }
    },
    {
      sequelize,
      modelName: "ocrTmplMgmt",
      tableName: "TB_RA_PT_OR_TMPLMGMT",
      freezetableName: true,
      timestamps: true
    }

  );
};
