const Sequelize = require("sequelize");

module.exports = (database) => {
  const sequenceName = "SEQ_SGSTNO";
  database.models.ocrTmplSgst.sequence = () => {
    return database.query(`SELECT NEXT VALUE FOR ${sequenceName}`,
      { type: Sequelize.QueryTypes.SELECT, plain: true }).then((result) => {
      return Promise.resolve(result[""]);
    });
  };
};
