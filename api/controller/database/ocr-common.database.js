const ocrCommonFields = ["userUid", "userNm", "ariBrc", "ariBrnm", "createdAt", "updatedAt", "deletedAt", "delYn"];

module.exports = ocrCommonFields;
