require("dotenv").config();

// console.log("process.env", process.env);

let {
  // eslint-disable-next-line prefer-const
  DB_SERVICE_HOST, DB_SERVICE_PORT, DB_ENV_DB, DB_USER, DB_PASSWORD
} = process.env;
DB_USER = DB_USER.trim().replace("\\n", "");
DB_PASSWORD = DB_PASSWORD.trim().replace("\\n", "");

const fs = require("fs");
// const { ProxyAuthenticationRequired } = require("http-errors");

const path = require("path");
const Sequelize = require("sequelize");

const database = new Sequelize(DB_ENV_DB, DB_USER, DB_PASSWORD, {
  host: DB_SERVICE_HOST,
  port: DB_SERVICE_PORT,
  dialect: "mssql",
  logging: false, // logging: console.log,
  dialectOptions: {
    options: {
      requestTimeout: 180000
    }
  },
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
});

// MSSQL bug fix
Sequelize.DATE.prototype._stringify = function _stringify(date, options) {
  return this._applyTimezone(date, options).format("YYYY-MM-DD HH:mm:ss.SSS");
};

database.Op = Sequelize.Op;
database.models = {};
const modelDir = path.join(__dirname, "../../model");
fs.readdirSync(modelDir).forEach((file) => {
  const model = require(path.join(modelDir, file))(database);
  database.models[model.name] = model;
});

// tmplId to ocrRqst
database.models.ocrTmplMgmt.hasMany(database.models.ocrRqst, { foreignKey: "TMPL_UUID" });
database.models.ocrRqst.belongsTo(database.models.ocrTmplMgmt, { foreignKey: "TMPL_UUID", targetKey: "tmplUid" });

// for query
database.paginationToOptions = (pagination) => {
  const paginationOptions = {
    order: null,
    offset: 0,
    limit: 10
  };
  // console.log("pagination", pagination);
  if (pagination == null) {
    return paginationOptions;
  }
  const paginationParse = JSON.parse(pagination);
  // console.log("paginationParse", paginationParse);
  if (paginationParse.sortBy && paginationParse.descending) {
    paginationOptions.order = [[paginationParse.sortBy, paginationParse.descending == true ? "DESC" : "ASC"]];
  }

  if (paginationParse.itemsPerPage) {
    paginationOptions.limit = paginationParse.itemsPerPage;
  }

  if (paginationParse.page && paginationParse.itemsPerPage) {
    paginationOptions.offset = (paginationParse.page - 1) * paginationParse.itemsPerPage;
  }

  if (paginationOptions.limit == 0) {
    delete paginationOptions.limit;
  }

  return paginationOptions;
};

require("./ocr-rqst.database.js")(database);
require("./ocr-tmpl-sgst.database.js")(database);
require("./ocr-tmpl-mgmt.database.js")(database);

module.exports = database;
