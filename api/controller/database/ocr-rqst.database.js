const Sequelize = require("sequelize");

module.exports = (database) => {
  const sequenceName = "SEQ_RQSTNO";
  database.models.ocrRqst.sequence = () => {
    return database.query(`SELECT NEXT VALUE FOR ${sequenceName}`,
      { type: Sequelize.QueryTypes.SELECT, plain: true }).then((result) => {
      return Promise.resolve(result[""]);
    });
  };

  database.models.ocrRqst.selectStatistics = (userUid) => {
    let selectSql = "";
    if (userUid) {
      selectSql = `
        SELECT 'accumulate' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
         WHERE PTL_USR_UUID = $userUid
        UNION ALL
        SELECT 'today' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        WHERE RQST_RG_DTM  >= CAST(GETDATE() AS DATE)
          AND PTL_USR_UUID = $userUid
        UNION ALL
        SELECT 'processing' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        WHERE OCR_STAT IN ('01','02')
          AND PTL_USR_UUID = $userUid
        UNION ALL
        SELECT 'error' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        WHERE OCR_STAT = '04'
          AND PTL_USR_UUID = $userUid
      `;
    } else {
      selectSql = `
        SELECT 'accumulate' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        UNION ALL
        SELECT 'today' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        WHERE RQST_RG_DTM  >= CAST(GETDATE() AS DATE)
        UNION ALL
        SELECT 'processing' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        WHERE OCR_STAT IN ('01','02')
        UNION ALL
        SELECT 'error' id, COUNT(1) number
          FROM TB_RA_PT_OR_OCRRQST
        WHERE OCR_STAT = '04'
      `;
    }
    return database.query(selectSql,
      {
        type: Sequelize.QueryTypes.RAW,
        plain: true,
        bind: { userUid }
      }).then((result) => {
      return Promise.resolve(result[0]);
    });
  };

  database.models.ocrRqst.getOcrNewRequestOne = () => {
    const selectSql = `
    SELECT TOP 1
           OCR_RQST_UUID   AS ocrRqstUid
          ,TMPL_UUID       AS tmplUid
          ,IMG_FILE_NAME   AS imgFileName
          ,IMG_FILE_TYPE   AS imgFileType
          ,IMG_PAGE_CNT    AS imgPageCnt
          ,RQST_RG_DTM     AS createdAt
          ,IMG_ORG_DATA    AS imgOrgData
          ,OCR_STAT        AS ocrStat
      FROM TB_RA_PT_OR_OCRRQST
     WHERE (
            OCR_STAT IN ('01', '05') OR
            (OCR_STAT = '02' AND RQST_MOD_DTM < DateADD(mi, -60, Current_TimeStamp))
           )
      `;

    return database.query(selectSql,
      {
        type: Sequelize.QueryTypes.RAW,
        plain: true,
        bind: { curDate: new Date() }
      }).then((result) => {
      return Promise.resolve(result[0][0]);
    });
  };
};
