const express = require("express");
const stream = require("stream");
const Message = require("./message.js");
const error = require("./error.js");
const database = require("../controller/database/database.js");

express.Message = Message;
express.session = {};
express.logs = [];

/*
 * name Response 반환 함수
 * params: status: Int, json: Object, e: Error, user: User
 */
express.response.return = function (status, json, e, user) {
  let userUid = null;
  let userId = "Unknown";
  let copDsc = "";

  if (this.req.session.robot) {
    userUid = this.req.session.robot.userUid;
    userId = this.req.session.robot.userId;
    copDsc = this.req.session.robot.copDsc;
  } else if (this.req.session.user) {
    userUid = this.req.session.user.userUid;
    userId = this.req.session.user.userId;
    copDsc = this.req.session.user.copDsc;
    this.header("Expired-At", this.req.session.user.timeout || -1);
  } else if (user) {
    userUid = user.userUid;
    userId = user.userId;
    copDsc = user.copDsc;
  }

  const method = this.req.method;
  const route = this.req.path;
  const args = ["PUT", "POST"].indexOf(method) >= 0 ? JSON.stringify(this.req.body) : JSON.stringify(this.req.params);
  const ipAdr = this.req.header["x-forwarded-for"] || this.req.connection.remoteAddress;
  // logger.info(userId, status, method, route, args, copDsc, ipAdr);

  const data = {
    userUid,
    route,
    method,
    status,
    content: e ? JSON.stringify(e) : null,
    executedAt: new Date(),
    args,
    ipAdr,
    copDsc
  };
  express.logs.push(data);
  return this.status(status).send(json);
};

/*
 * name Response 파일 반환 함수
 * params: status: Int, json: Object, e: Error, user: User
 */
express.response.returnFileStream = function (buffer) {
  let userUid = null;
  let userId = "Unknown";
  let copDsc = "";

  if (this.req.session.robot) {
    userUid = this.req.session.robot.userUid;
    userId = this.req.session.robot.userId;
    copDsc = this.req.session.robot.copDsc;
  } else if (this.req.session.user) {
    userUid = this.req.session.user.userUid;
    userId = this.req.session.user.userId;
    copDsc = this.req.session.user.copDsc;
    this.header("Expired-At", this.req.session.user.timeout || -1);
  }

  const method = this.req.method;
  const route = this.req.path;
  const args = ["PUT", "POST"].indexOf(method) >= 0 ? JSON.stringify(this.req.body) : JSON.stringify(this.req.params);
  const status = 200;
  const ipAdr = this.req.header["x-forwarded-for"] || this.req.connection.remoteAddress;
  // logger.info(userId, status, method, route, args, copDsc, ipAdr);

  const data = {
    userUid,
    route,
    method,
    status,
    content: e ? JSON.stringify(e) : null,
    executedAt: new Date(),
    args,
    ipAdr,
    copDsc
  };
  express.logs.push(data);

  const readable = stream.Readable();
  readable.on("close", () => {
    console.log("close");
  });

  readable._read = () => {};
  readable.push(buffer);
  readable.push(null);
  readable.pipe(this);
};

/*
 * name 성공반환
 * params:
 */
express.response.returnSuccess = function (message, user) {
  const status = 200;
  return this.return(status, message, null, user);
};

/*
 * name 에외반환
 * params:
 */
express.response.returnSuccessException = function (code) {
  const status = 200;
  const message = new Message(code, { message: error[code] });
  return this.return(status, message);
};

/*
 * name 세션타임아웃 반환
 * params:
 */
express.response.returnSessionTimeout = function () {
  const status = 401;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, error[status]);
};

/*
 * name 인증실패반환
 * params:
 */
express.response.returnNotAuthenticate = function () {
  const status = 403;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, error[status]);
};

/*
 * name API 없음 반환
 * params:
 */
express.response.returnNotFound = function () {
  const status = 404;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, error[status]);
};

/*
 * name 권한 없음 반환
 * params:
 */
express.response.returnNotAllow = function () {
  const status = 405;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, error[status]);
};

/*
 * name 중복로그인 반환
 * params:
 */
express.response.returnDuplicateLogin = function () {
  const status = 406;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, error[status]);
};

/*
 * name 서버오류 반환
 * params:
 */
express.response.returnServerError = function (e) {
  const status = 500;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, e);
};

/*
 * name 잘못된 파라미터 반환
 * params:
 */
express.response.returnInvalidParameter = function () {
  const status = 501;
  const message = Message(0, { message: error[status] });
  return this.return(status, message, error[status]);
};

/*
 * name 잘못된 파일 반환
 * params:
 */
express.response.returnInvalidAttachment = function (files) {
  const status = 502;
  const message = Message(0, { message: error[status], files });
  return this.return(status, message, error[status]);
};

express.response.returnVirusAttachment = function (files) {
  const status = 503;
  const message = Message(0, { message: error[status], files });
  return this.return(status, message, error[status]);
};

express.response.returnEncryptFailedAttachment = function (files) {
  const status = 504;
  const message = Message(0, { message: error[status], files });
  return this.return(status, message, error[status]);
};

express.disabled = (req, res, next) => {
  return res.returnNotFound();
};

/*
 * name 인증 및 권한 검증
 * params:
 */
express.isAuthenticated = (menuCode, eventCode) => {
  return (req, res, next) => {
    console.log(`Check authenticate Menucoe : ${menuCode}, eventCode : ${eventCode}`);
    console.log("session user is", req.session.user == null ? "none." : req.session.user.userUid);
    // console.log("config", config);

    if (req.session.user == null) {
      const returnMsg = {};
      returnMsg.userUid = "1000";
      returnMsg.userId = "developer1";
      returnMsg.userNm = "개발자1";
      returnMsg.ariBrc = "B0001";
      returnMsg.ariBrnm = "기술팀";
      req.session.user = returnMsg;

      // return res.returnNotAuthenticate();
    }
    // const userUid = req.session.user.userUid;

    // if (express.sessions[userUid] && express.sessions[userUid] != req.session.user.sessionKey) {
    //   return res.returnDuplicateLogin();
    // }

    Promise.resolve()
      .then(() => {
        if (config.sessionTimeout > -1) {
          if (req.session.user.timeout == null) {
            req.session.user.timeout = new Date().getTime() + 1000 * 60 * 30; // check
          }

          if (new Date().getTime() < req.session.user.timeout) {
          // extend session timeout
            req.session.user.timeout = new Date().getTime() + 1000 * 60 * 30; // check
            return Promise.resolve();
          }

          return Promise.reject("timeout");
        }
      })
      .then(() => {
        // check user and role ...
        // return database.models.user.findOne({
        // })
        return next();
      })
      .catch((e) => {
        if (e == "timeout") {
          return res.returnSessionTimeout();
        }
        return res.returnServerError(e);
      });
  };
};

module.exports = express;
