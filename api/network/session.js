const session = require("express-session");

module.exports = session({
  secret: "!@#$%^&*",
  resave: false,
  saveUninitialized: true,
  cookie: {
    maxAge: 1000 * 60 * 60 * 24 // cookie expr time
  }
});
