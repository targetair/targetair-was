const error = {};

// Backend System
error[401] = "세션 만료";
error[403] = "인증 실패";
error[404] = "API 실패";
error[405] = "권한 없음";
error[406] = "중복 로그인";
error[500] = "서버 오류";
error[501] = "잘못된 파라미터";
error[502] = "잘못된 첨부파일";
error[503] = "바이러스검출";
error[504] = "암호화 실패";

// Backend service
error[1001] = "이미 사용중인 계정";
error[1002] = "계정 또는 패스워드 오류";
error[1003] = "사용자 인증 실패"; // 없음
error[1004] = "사용자 인증 실패"; // 조직없음
error[2001] = "채워지지 않은 항목존재";
error[2002] = "해당 단계가 아님";
error[2003] = "결재 진행중";
error[3001] = "조회기간이 숫자인지 확인";
error[3002] = "조회기간 범위 확인";
error[3003] = "인증서 만료";
error[3004] = "인증서 혹은 패스워드 확인";
error[4001] = "서버 오류";
error[4002] = "서버 오류로 인한 실행 실패";

// database
error[8001] = "삭제시 Relation 오류";
// orchestrator
error[9001] = "";
// Backend 기타
error[9999] = "삭제불가";

module.exports = error;
