const Message = (code = 0, data = {}, total = 0) => {
  this.code = code;
  this.data = data;
  this.total = total;
  return this;
};

module.exports = Message;
