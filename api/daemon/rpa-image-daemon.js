/* eslint-disable max-len */
/* eslint-disable no-await-in-loop */
require("dotenv").config();

const { RUN_PLATFORM, USE_OCR_ENGINE_STUB } = process.env;
const runPlatform = RUN_PLATFORM || "LINUX";
const runApiStub = USE_OCR_ENGINE_STUB || "NO";

const fs = require("fs");
// const { result } = require("lodash");
const path = require("path");
const pdf2img = require("pdf2img");

let pdfPoppler = null;
if (runPlatform != "LINUX") {
  
}
const { promisify } = require("util");
const ocrHttp = require("../../plugins/http-ocr-engine.js"); // nh

const fsPromises = fs.promises;

const pdf2imgConvertAsync = promisify(pdf2img.convert);

// const Sequelize = require("sequelize");
// const { resolve } = require("path");
// const express = require("../network/express.js");
const database = require("../controller/database/database.js");

console.log("runPlatform", runPlatform);
console.log("runApiStub", runApiStub);

const pdfWorkPath = path.join(__dirname, "..", "..", "pdfwork");
// const testPdfFile = "/Users/bigfarm/MyData/WinShare/NH/PDF-Sample/file-sample_150kB_4p.pdf";
// const testPdfFile = "/Users/bigfarm/MyData/WinShare/NH/PDF-Sample/nh_ci.png";
// console.log("path object : ");
// console.log(path);
// console.log(`__dirname : ${__dirname}`);
console.log(`pdfWorkPath : ${pdfWorkPath}`);
// console.log(`pdf dirname : ${path.dirname(testPdfFile)}`);
// console.log(`pdf basename : ${path.basename(testPdfFile)}`);
// console.log(`pdf name : ${path.parse(testPdfFile).name}`);
// console.log(`pdf extname : ${path.extname(testPdfFile)}`);

const pdf2imgOption = {
  type: "png", // png or jpg, default jpg
  size: 3000, // default 1024
  density: 300, // default 600
  // eslint-disable-next-line max-len
  outputdir: pdfWorkPath, // output folder, default null (if null given, then it will create folder name same as file name)
  outputname: "output", // output file name, dafault null (if null given, then it will create image name same as input name)
  page: null, // convert selected page, default null (if null given, then it will convert all pages)
  quality: 100 // jpg compression quality, default: 100
};

const pdfPopplerOption = {
  format: "png", // FORMATS = ['png', 'jpeg', 'tiff', 'pdf', 'ps', 'eps', 'svg']
  out_dir: pdfWorkPath,
  out_prefix: "file",
  scale: 3000,
  page: null
};

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

async function clearJobDirectory() {
  try {
    if (fs.existsSync(pdfWorkPath)) {
      console.log("     Job directory clear start.");
      const files = await fsPromises.readdir(pdfWorkPath);
      const unlinkPromises = files.map((filename) => {
        console.log(`     unlink file : ${filename}`);
        return fsPromises.unlink(`${pdfWorkPath}/${filename}`);
      });

      await Promise.all(unlinkPromises);
      console.log("     Done.");
    } else {
      fs.mkdirSync(pdfWorkPath);
      console.log("     Job directory created.");
    }
  } catch (err) {
    console.error(err.message);
    throw err;
  }
}

async function convertPdfImage(pdfFile) {
  let result = null;
  try {
    pdf2imgOption.outputname = path.parse(pdfFile).name;
    pdf2img.setOptions(pdf2imgOption);

    const start = new Date();
    result = await pdf2imgConvertAsync(pdfFile);
    const end = new Date() - start;
    console.log("     PDF2IMG Covert execution time: %dms", end);
    console.log(result);
  } catch (err) {
    console.error(err);
    throw err;
  }
  return result;
}

async function convertPdfPoppler(pdfFile) {
  let result = null;

  try {
    pdfPopplerOption.out_prefix = path.parse(pdfFile).name;
    const start = new Date();

    let pdfinfo = null;
    await pdfPoppler.info(pdfFile)
      .then((pdfinfoResult) => {
        pdfinfo = pdfinfoResult;
        console.log(pdfinfo);
      })
      .catch((error) => {
        throw error;
      });

    await pdfPoppler.convert(pdfFile, pdfPopplerOption)
      .then(() => {
        console.log("  => Successfully converted.");
      })
      .catch((error) => {
        throw error;
      });

    const end = new Date() - start;
    console.log("     PDF2IMG Covert execution time: %dms", end);
    result = {
      result: "success",
      message: []
    };
    const pdfPageSize = parseInt(pdfinfo.pages, 10);
    const digitCountForPages = pdfinfo.pages.length;

    for (let idx = 0; idx < pdfPageSize; idx += 1) {
      const filePageString = (idx + 1).toString().padStart(digitCountForPages, "0");
      const fileExtName = pdfPopplerOption.format == "jpeg" ? "jpg" : pdfPopplerOption.format;
      const pageInfo = {
        page: idx + 1,
        name: `${pdfPopplerOption.out_prefix}-${filePageString}.${fileExtName}`,
        path: `${pdfWorkPath}${path.sep}${pdfPopplerOption.out_prefix}-${filePageString}.${fileExtName}`
      };
      result.message.push(pageInfo);
    }
    console.log(result);
  } catch (err) {
    console.error(`[[${err}]]`);
    throw err;
  }
  // console.log(result);
  return result;
}

async function fileSaveFromBuffer(imgFileName, base64String) {
  const fullFileName = pdfWorkPath + path.sep + imgFileName;

  try {
    const buf = Buffer.from(base64String, "base64");
    await fsPromises.writeFile(fullFileName, buf);
  } catch (err) {
    console.error(err.message);
    throw err;
  }

  return fullFileName;
}

function getApiStubResult() {
  return {
    product_info: {
      count1: [
        "6613",
        "6614",
        "6615",
        "6616"
      ],
      count2: [
        "14",
        "15",
        "110",
        "120"
      ],
      count3: [
        "92582",
        "3333",
        "2323",
        "5555"
      ],
      product_name: [
        "스프라이트(업소용)QR355ml",
        "스프라이트(업소용)QR500ml",
        "스프라이트(업소용)QR700ml",
        "스프라이트(업소용)QR1000ml"
      ]
    },
    supply_info: {
      customer_name: "대구경북능금농협음료가공공장",
      customer_num: "509-82-01424",
      shipping_date: "20/07/24",
      supplier_name: "쥐테크팩솔루션하남공장",
      supplier_num: "110-85-37768"
    }
  };
}

async function ocrEngineCall(apiUrl, imageBase64) {
  const start = new Date();

  let result = null;

  if (runApiStub == "YES") {
    const max = 1000;
    const min = 100;
    const randSec = Math.floor(Math.random() * (max - min)) + min;
    console.log(`randSec = ${randSec}`);

    if (randSec % 5 == 0) { throw new Error("Test Error"); } // 20% 확율로 테스트 에러 발생
    await sleep(randSec);
    result = getApiStubResult();
  } else {
    const params = {
      image: imageBase64
    };
    console.log("  => OCR Engine call start.");
    const apiResult = await ocrHttp.ocrEngineApiCall(apiUrl, params);
    result = apiResult.data;
    console.log("  => OCR Engine call end.");
    console.log("Api call result", result);
  }

  const end = new Date() - start;
  console.log("     OCR Engine call execution time: %dms", end);
  return result;
}

async function ocrImageProcess(page, templateDetail, apiUrl, imageBase64) {
  try {
    const resultMap = JSON.parse(JSON.stringify(templateDetail.tmplOcrItem));
    resultMap.forEach((f) => {
      if (!f.itemApiName1 && !f.itemApiName2) throw new Error("템플릿 API 항목 정보가 누락 되었습니다.");
    });

    const ocrCallResult = await ocrEngineCall(apiUrl, imageBase64);
    // console.log("ocrCallResult", ocrCallResult);

    resultMap.forEach((f) => {
      if (f.itemApiName1 && f.itemApiName2) {
        f.itemData = ocrCallResult[f.itemApiName1][f.itemApiName2];
      } else {
        f.itemData = ocrCallResult[f.itemApiName1];
      }

      // if (f.hasOwnProperty("itemApiName1")) delete f.itemApiName1;
      if (Object.prototype.hasOwnProperty.call(f, "itemApiName1")) delete f.itemApiName1;

      // if (f.hasOwnProperty("itemApiName2")) delete f.itemApiName2;
      if (Object.prototype.hasOwnProperty.call(f, "itemApiName2")) delete f.itemApiName2;
    });

    return {
      pageNo: page,
      ocrSuccess: true,
      pageResultData: resultMap
    };
  } catch (err) {
    console.error(err.message);
    return {
      pageNo: page,
      ocrSuccess: false,
      pageResultData: []
    };
  }
}

async function getOcrNewRequestOne() {
  const result = await database.models.ocrRqst.getOcrNewRequestOne();
  // console.log("result", result);
  return result;
}

async function updateOcrRequestStatus(obj) {
  const updateData = { ocrStat: obj.ocrStat };
  if (obj.ocrRsltOrigItem) {
    updateData.ocrRsltOrigItem = obj.ocrRsltOrigItem;
  }
  if (obj.ocrRsltModItem) {
    updateData.ocrRsltModItem = obj.ocrRsltModItem;
  }
  if (obj.cmplAt) {
    updateData.cmplAt = obj.cmplAt;
  }

  await database.models.ocrRqst.update(
    updateData,
    { where: { ocrRqstUid: obj.ocrRqstUid } }
  );
}

// OCR Template 상세조회
async function getOcrTemplateDetail(tmplUid) {
  const selectAttributes = ["tmplUid", "tmplNm", "tmplDesc", "tmplStat", "tmplOcrItemCnt", "tmplOcrItem", "imgFileType", "tmplEfctDtm", "tmplExprDtm", "ocrApiUrl"];
  const result = await database.models.ocrTmplMgmt.findOne({
    attributes: selectAttributes,
    where: {
      tmplUid
    }
  });

  result.tmplOcrItem = JSON.parse(result.tmplOcrItem);

  return result;
}

async function processNewRequest() {
  const x = true;

  try {
    await clearJobDirectory();
  } catch (err) {
    console.error(err);
    throw err;
  }

  // 미처리된 request를 1건 읽어서 처리한다.
  // 없으면 10초 대기후 재조회를 수행한다.

  while (x) {
    try {
      const record = await getOcrNewRequestOne();
      try {
        if (!record || !record.ocrRqstUid) {
          console.debug("No more new request. wait 10 sec.");
          await sleep(10000);
        } else {
          console.log(`>>> Request ocr processing start. Req ID : ${record.ocrRqstUid} >>>`);
          // 진행중 업데이트
          await updateOcrRequestStatus({ ocrRqstUid: record.ocrRqstUid, ocrStat: "02" });
          const templateDetail = await getOcrTemplateDetail(record.tmplUid);

          console.log(`  => fileType : ${record.imgFileType}, fileName : ${record.imgFileName}`);

          const resultSet = [];

          // pdf 경우 파일로 저장후 각 페이지를 jpg로 변환한다.
          if (record.imgFileType === "application/pdf") {
            // pdf 경우 pdf 파일 저장 -> 페이지별 image 변환 -> 변환이미지 ocr 인식처리 순으로 진행 한다.
            // 작업 디렉토리 정리
            await clearJobDirectory();
            // 파일 저장
            const saveFileName = await fileSaveFromBuffer(record.imgFileName, record.imgOrgData);

            // 이미지 변환
            // const result = await convertPdfImage(saveFileName);
            const result = await (runPlatform == "LINUX" ? convertPdfImage(saveFileName) : convertPdfPoppler(saveFileName));

            if (result.result != "success") {
              throw new Error("Pdf to image covert failed.");
            }

            // ocr engine process
            for (let idx = 0; idx < result.message.length; idx += 1) {
              console.log(`     page : ${result.message[idx].page} process start..`);
              const imageBase64 = fs.readFileSync(result.message[idx].path, { encoding: "base64" });
              // await fileSaveFromBuffer(`${result.message[idx].page}-out.png`, imageBase64);
              console.log(`pdf base64 image = ${imageBase64.substring(0, 30)}`);

              resultSet.push(
                await ocrImageProcess(
                  result.message[idx].page,
                  templateDetail,
                  templateDetail.ocrApiUrl,
                  imageBase64
                )
              );
            }
          } else {
            // image 인 경우 직접 처리한다.
            resultSet.push(
              await ocrImageProcess(
                1,
                templateDetail,
                templateDetail.ocrApiUrl,
                record.imgOrgData
              )
            );
          }

          await updateOcrRequestStatus({
            ocrRqstUid: record.ocrRqstUid,
            ocrStat: resultSet.filter((f) => f.ocrSuccess).length <= 0 ? "04" : "03",
            ocrRsltOrigItem: JSON.stringify(resultSet),
            ocrRsltModItem: JSON.stringify(resultSet),
            cmplAt: new Date()
          });
          console.log("  => Updated db status.");
          // console.log(JSON.stringify(resultSet, null, 4));
          console.log(`<<< Request ocr processing start. Req ID : ${record.ocrRqstUid} <<<`);
        }
        // if (x) break;
      } catch (err) {
        await updateOcrRequestStatus({ ocrRqstUid: record.ocrRqstUid, ocrStat: "04" });
        throw err;
      }
    } catch (err) {
      // do not throw.
      console.error(err.message);
      await sleep(10000);
    }
    // await sleep(10000);
  }
}

// clearJobDirectory();
// convertPdfPoppler(testPdfFile);
processNewRequest();

module.exports = {
};
