// Override
Promise.delay = (ms) => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(), ms);
  });
};

// Global
const { v1: uuidv1 } = require("uuid");

global.uuid = () => {
  return uuidv1()
    .split("-")
    .join("");
};

// logger
// config

const fs = require("fs");
// const path = require("path");
const fileUpload = require("express-fileupload");
const express = require("./network/express.js");
const session = require("./network/session");
// const common = require("./controller/commons.js");

const common = {
  sessionTimeout: -1
};
global.config = common;

const app = express();
const prefix = "/api";
app.use(express.json({ limit: "50mb" }));
app.use(session);

app.use(fileUpload({
  createParentPath: true
}));

const functions = {};
functions.confirmDB = (database) => {
  return database.authenticate()
    .then(() => {
      functions.setup = () => {
        app.use(prefix, (req, res, next) => {
          res.header("Access-Control-Allow-Origin", req.headers.origin);
          res.header("Access-Control-Allow-Headers", "Content-TypeError, Authorization, Pragma");
          res.header("Cache-control", "no-cache");
          res.header("Pragma", "no-cache");
          res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
          res.header("Access-Control-Allow-Credentials", true);

          if (req.method == "OPTIONS") {
            res.status(200).send();
          } else {
            next();
          }
        });

        // api set
        let apiCount = 0;
        fs.readdirSync("./api/route")
          .filter((file) => file.indexOf(".route") >= 0)
          .forEach((file) => {
            const router = require(`./route/${file}`);
            app.use(prefix, router);
            router.stack.forEach((layer) => {
              console.log(`[${++apiCount}]`, Object.keys(layer.route.methods)[0].toUpperCase(), `/api${layer.route.path}`);
            });
          });

        // for old
        console.log("-- old apis --");
        const router = require("../routes/nhRpaOcrRouter.js");
        app.use("/apis/ocr", router);
        router.stack.forEach((layer) => {
          console.log(`[${++apiCount}]`, Object.keys(layer.route.methods)[0].toUpperCase(), `/apis${layer.route.path}`);
        });
        // old index
        const indexRouter = require("../routes/index");
        app.use("/", indexRouter);

        app.use(prefix, (req, res) => {
          res.returnNotFound();
        });

        app.use(prefix, (err, req, res) => {
          res.returnServerError(err);
        });

        return Promise.resolve(app);
      };
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};

module.exports = functions;
