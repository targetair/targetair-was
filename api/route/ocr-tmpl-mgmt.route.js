// const moment = require("moment");
const Sequelize = require("sequelize");
const express = require("../network/express.js");
const database = require("../controller/database/database.js");
const ocrCommonFields = require("../controller/database/ocr-common.database.js");

const Message = express.Message;
const app = express.Router();

// OCR Template 리스트 조회
app.get("/ocr/getOcrTemplateList", express.isAuthenticated("test", "test"), (req, res) => {
  const selectAttributes = ["tmplUid", "tmplNm", "tmplDesc", "tmplStat", "tmplOcrItemCnt", "tmplOcrItem", "imgFileType", "tmplEfctDtm", "tmplExprDtm", "ocrApiUrl"];
  database.models.ocrTmplMgmt.findAll({
    attributes: selectAttributes,
    order: [["tmplUid", "DESC"]]
  })
    .then((result) => {
      if (result.length > 0) {
        for (let idx = 0; idx < result.length; idx += 1) {
          result[idx].tmplOcrItem = JSON.parse(result[idx].tmplOcrItem);
        }
      }

      const message = Message(0, result);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// OCR 사용가능 Template 리스트 조회
app.get("/ocr/getOcrTemplateAvailableList", express.isAuthenticated("test", "test"), (req, res) => {
  const selectAttributes = ["tmplUid", "tmplNm", "tmplDesc", "tmplStat"];
  database.models.ocrTmplMgmt.findAll({
    attributes: selectAttributes,
    where: {
      tmplStat: "02"
    },
    order: [["tmplUid", "ASC"]]
  })
    .then((result) => {
      const message = Message(0, result);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// OCR Template 상세조회
app.get("/ocr/getOcrTemplateDetail", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  if (!reqItem || !reqItem.tmplUid) {
    res.returnInvalidParameter();
    return;
  }
  const selectAttributes = ["tmplUid", "tmplNm", "tmplDesc", "tmplStat", "tmplOcrItemCnt", "tmplOcrItem", "imgFileType", "imgOrgData", "tmplEfctDtm", "tmplExprDtm", "ocrApiUrl"];
  database.models.ocrTmplMgmt.findOne({
    attributes: selectAttributes.concat(ocrCommonFields),
    where: {
      tmplUid: reqItem.tmplUid
    }
  })
    .then((result) => {
      result.tmplOcrItem = JSON.parse(result.tmplOcrItem);
      const message = Message(0, result);
      res.returnSuccess(message);
    })
    .catch((e) => {
      console.error(e);
      res.returnServerError(e);
    });
});

// 템플릿 등록
app.post("/ocr/createOcrTemplate", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;
  const reqFiles = req.files;

  if (!reqFiles || !reqFiles.uploadFile) {
    res.returnInvalidAttachment();
    return;
  }

  const user = req.session.user;

  database.models.ocrTmplMgmt.sequence()
    .then((result) => {
      console.log(`New sequence : ${result}`);
      const insertData = {
        tmplUid: result,
        userUid: user.userUid,
        userNm: user.userNm,
        ariBrc: user.ariBrc,
        ariBrnm: user.ariBrnm,
        delYn: "N",
        tmplId: reqItem.tmplId,
        tmplNm: reqItem.tmplNm,
        tmplDesc: reqItem.tmplDesc,
        tmplOcrItemCnt: reqItem.tmplOcrItemCnt,
        tmplOcrItem: reqItem.tmplOcrItem,
        imgFileType: reqItem.imgFileType,
        imgOrgData: reqFiles.uploadFile.data.toString("base64"),
        tmplEfctDtm: null,
        tmplExprDtm: null,
        ocrApiUrl: reqItem.ocrApiUrl,
        tmplStat: "01"
      };

      database.models.ocrTmplMgmt.create(insertData)
        .then((resultInsert) => {
          const message = Message(0, resultInsert);
          res.returnSuccess(message);
        })
        .catch((e) => {
          // console.error(e);
          res.returnServerError(e);
        });
    }).catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// 템플릿 변경
app.post("/ocr/updateOcrTemplate", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;
  const reqFiles = req.files;

  if (!reqFiles || !reqFiles.uploadFile) {
    reqItem.imgOrgData = null;
  } else {
    reqItem.imgOrgData = reqFiles.uploadFile.data.toString("base64");
  }

  const updateData = {};

  if (reqItem.tmplNm) updateData.tmplNm = reqItem.tmplNm;
  if (reqItem.tmplDesc) updateData.tmplDesc = reqItem.tmplDesc;
  if (reqItem.tmplOcrItemCnt) updateData.tmplOcrItemCnt = reqItem.tmplOcrItemCnt;
  if (reqItem.tmplOcrItem) updateData.tmplOcrItem = reqItem.tmplOcrItem;
  if (reqItem.ocrApiUrl) updateData.ocrApiUrl = reqItem.ocrApiUrl;
  if (reqItem.imgOrgData) {
    updateData.imgOrgData = reqItem.imgOrgData;
    updateData.imgFileType = reqItem.imgFileType;
  }

  database.models.ocrTmplMgmt.update(
    updateData,
    { where: { tmplUid: reqItem.tmplUid } }
  )
    .then((resultUpdate) => {
      const message = Message(0, resultUpdate);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// 템플릿 상태 변경
app.post("/ocr/updateOcrTemplateStatus", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;

  if (!reqItem || !reqItem.tmplUid || !reqItem.tmplStat) {
    res.returnInvalidParameter();
    return;
  }

  const updateData = { tmplStat: reqItem.tmplStat };
  if (reqItem.tmplStat == "02") {
    updateData.tmplEfctDtm = new Date();
    updateData.tmplExprDtm = null;
  } else if (reqItem.tmplStat == "03") {
    updateData.tmplExprDtm = new Date();
  }

  database.models.ocrTmplMgmt.update(
    updateData,
    { where: { tmplUid: reqItem.tmplUid } }
  )
    .then((resultUpdate) => {
      const message = Message(0, resultUpdate);
      res.returnSuccess(message);
    })
    .catch((e) => {
      console.error(e);
      res.returnServerError(e);
    });
});

module.exports = app;
