// const moment = require("moment");
const Sequelize = require("sequelize");
const express = require("../network/express.js");
const database = require("../controller/database/database.js");
const ocrCommonFields = require("../controller/database/ocr-common.database.js");
// const orchestratorFunctions = require("../controller/orchestrator/orchestrator.js");
// const upload = require("../controller/upload.js");
const Op = Sequelize.Op;

const Message = express.Message;
const app = express.Router();

// ocr 요청 리스트 조회 (사용자)
app.get("/ocr/getOcrRequestList", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  // console.log("reqItem", reqItem);
  // console.log("req", req.session);

  const pagination = reqItem && reqItem.pagination ? reqItem.pagination : null;

  let searchParam = {};
  if (reqItem && reqItem.search) {
    const searchParse = JSON.parse(reqItem.search);
    searchParam = {
      searchText: searchParse.searchText ? searchParse.searchText || "" : "",
      searchStatus: searchParse.searchStatus ? searchParse.searchStatus || "" : "",
    };
  }

  const userUid = req.session.user.userUid;
  const paging = database.paginationToOptions(pagination);
  console.log("paging", paging);

  const where = {};
  where[Op.and] = [{ userUid }];

  // where.userUid = userUid;
  if (searchParam.searchStatus) {
    where[Op.and].push({ ocrStat: searchParam.searchStatus });
  }
  if (searchParam.searchText) {
    where[Op.or] = [
      { "$ocrRqst.PUPS_USE$": { [Op.like]: `%${searchParam.searchText}%` } },
      { "$ocrRqst.USRNM$": { [Op.like]: `%${searchParam.searchText}%` } },
      { "$ocrTmplMgmt.TMPLNM$": { [Op.like]: `%${searchParam.searchText}%` } },
    ];
  }
  // console.log("where", where);

  const selectAttributes = ["ocrRqstUid", "tmplUid", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "ocrStat", "cmplAt", "retryCnt", "ocrRsltModItem"];
  database.models.ocrRqst.findAndCountAll({
    attributes: selectAttributes.concat(ocrCommonFields),
    where,
    offset: paging.offset,
    limit: paging.limit,
    include: [
      {
        model: database.models.ocrTmplMgmt,
        attributes: ["tmplNm"],
        required: true
      }
    ],
    order: [["createdAt", "DESC"]]
  })
    .then((result) => {
      console.log(`getOcrRequestList result total ${result.count}, fetch count : ${result.rows.length}`);
      if (result.ocrRsltModItem) {
        result.ocrRsltModItem = JSON.parse(result.ocrRsltModItem);
      }

      result.rows.forEach((element) => {
        if (element.ocrRsltOrigItem) {
          element.ocrRsltOrigItem = JSON.parse(element.ocrRsltOrigItem);
        }
        if (element.ocrRsltModItem) {
          element.ocrRsltModItem = JSON.parse(element.ocrRsltModItem);
        }
      });

      const message = Message(0, result.rows, result.count);
      res.returnSuccess(message);
    })
    .catch((e) => {
      console.error(e);
      res.returnServerError(e);
    });
});

// ocr 요청 리스트 조회 (관리자)
app.get("/ocr/getOcrRequestListAdmin", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  console.log("reqItem", reqItem);
  // console.log("req", req.session);

  const pagination = reqItem && reqItem.pagination ? reqItem.pagination : null;

  let searchParam = {};
  if (reqItem && reqItem.search) {
    const searchParse = JSON.parse(reqItem.search);
    searchParam = {
      searchText: searchParse.searchText ? searchParse.searchText || "" : "",
      searchStatus: searchParse.searchStatus ? searchParse.searchStatus || "" : "",
    };
  }

  const paging = database.paginationToOptions(pagination);
  // console.log("paging", paging);

  const where = {};

  // where.userUid = userUid;
  if (searchParam.searchStatus) {
    where[Op.and] = [{ ocrStat: searchParam.searchStatus }];
  }
  if (searchParam.searchText) {
    where[Op.or] = [
      { "$ocrRqst.PUPS_USE$": { [Op.like]: `%${searchParam.searchText}%` } },
      { "$ocrRqst.USRNM$": { [Op.like]: `%${searchParam.searchText}%` } },
      { "$ocrTmplMgmt.TMPLNM$": { [Op.like]: `%${searchParam.searchText}%` } },
    ];
  }

  const selectAttributes = ["ocrRqstUid", "tmplUid", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "ocrStat", "cmplAt", "retryCnt", "ocrRsltModItem"];
  database.models.ocrRqst.findAndCountAll({
    attributes: selectAttributes.concat(ocrCommonFields),
    where,
    offset: paging.offset,
    limit: paging.limit,
    include: [
      {
        model: database.models.ocrTmplMgmt,
        attributes: ["tmplNm"],
        required: true
      }
    ],
    order: [["createdAt", "DESC"]]
  })
    .then((result) => {
      // console.log(`getOcrRequestListAdmin result total ${result.count}, fetch count : ${result.rows.length}`);
      result.rows.forEach((element) => {
        if (element.ocrRsltOrigItem) {
          element.ocrRsltOrigItem = JSON.parse(element.ocrRsltOrigItem);
        }
        if (element.ocrRsltModItem) {
          element.ocrRsltModItem = JSON.parse(element.ocrRsltModItem);
        }
      });
      const message = Message(0, result.rows, result.count);
      res.returnSuccess(message);
    })
    .catch((e) => {
      res.returnServerError(e);
    });
});

// ocr 요청 현황 조회 (사용자)
app.get("/ocr/getOcrRequestStatistics", express.isAuthenticated("test", "test"), (req, res) => {
  const userUid = req.session.user.userUid;
  database.models.ocrRqst.selectStatistics(userUid)
    .then((result) => {
      console.log("getOcrRequestStatistics result", result);
      const message = Message(0, result);
      res.returnSuccess(message);
    })
    .catch((e) => {
      res.returnServerError(e);
    });
});

// ocr 요청 현황 조회 (관리자)
app.get("/ocr/getOcrRequestStatisticsAdmin", express.isAuthenticated("test", "test"), (req, res) => {
  database.models.ocrRqst.selectStatistics()
    .then((result) => {
      // console.log("getOcrRequestStatisticsAdmin result", result);
      const message = Message(0, result);
      res.returnSuccess(message);
    })
    .catch((e) => {
      res.returnServerError(e);
    });
});

// ocr 신규 요청
app.post("/ocr/createOcrRequest", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;
  const reqFiles = req.files;

  if (!reqFiles || !reqFiles.uploadFile) {
    res.returnInvalidAttachment();
    return;
  }

  const user = req.session.user;

  database.models.ocrRqst.sequence()
    .then((result) => {
      console.log(`New sequence : ${result}`);
      const insertData = {
        ocrRqstUid: result,
        userUid: user.userUid,
        userNm: user.userNm,
        ariBrc: user.ariBrc,
        ariBrnm: user.ariBrnm,
        delYn: "N",
        tmplUid: reqItem.tmplUid,
        pupsUse: reqItem.pupsUse,
        imgFileName: reqItem.imgFileName,
        imgFileType: reqItem.imgFileType,
        imgPageCnt: reqItem.imgPageCnt,
        imgOrgData: reqFiles.uploadFile.data.toString("base64"),
        ocrStat: "01",
        retryCnt: 0
      };

      database.models.ocrRqst.create(insertData)
        .then((resultInsert) => {
          const message = Message(0, resultInsert);
          res.returnSuccess(message);
        })
        .catch((e) => {
          console.error(e.message);
          res.returnServerError(e);
        });
    }).catch((e) => {
      console.error(e.message);
      res.returnServerError(e);
    });
});

// ocr 요청 상세조회
app.get("/ocr/getOcrRequestDetail", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  if (!reqItem || !reqItem.ocrRqstUid) {
    res.returnInvalidParameter();
    return;
  }
  const selectAttributesRequest = ["ocrRqstUid", "tmplUid", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "ocrStat", "cmplAt", "retryCnt", "imgOrgData"];

  database.models.ocrRqst.findOne({
    attributes: selectAttributesRequest.concat(ocrCommonFields),
    where: { ocrRqstUid: reqItem.ocrRqstUid },
    include: [
      {
        model: database.models.ocrTmplMgmt,
        attributes: ["tmplNm"],
        required: true
      }
    ]
  })
    .then((resultRequest) => {
      const message = Message(0, resultRequest);
      res.returnSuccess(message);
    })
    .catch((e) => {
      res.returnServerError(e);
    });
});

// ocr 요청 결과조회
app.get("/ocr/getOcrRequestResult", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  if (!reqItem || !reqItem.ocrRqstUid) {
    res.returnInvalidParameter();
    return;
  }
  const selectAttributesRequest = ["ocrRqstUid", "tmplUid", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "ocrStat", "cmplAt", "ocrRsltOrigItem", "ocrRsltModItem", "retryCnt", "imgOrgData"];
  const selectAttributesTemplate = ["tmplUid", "tmplNm", "tmplDesc", "tmplStat", "tmplOcrItemCnt", "tmplOcrItem", "imgFileType", "imgOrgData", "tmplEfctDtm", "tmplExprDtm", "ocrApiUrl"];

  database.models.ocrRqst.findOne({
    attributes: selectAttributesRequest.concat(ocrCommonFields),
    where: { ocrRqstUid: reqItem.ocrRqstUid },
    include: [
      {
        model: database.models.ocrTmplMgmt,
        attributes: ["tmplNm"],
        required: true
      }
    ]
  })
    .then((resultRequest) => {
      if (resultRequest.ocrRsltOrigItem) {
        resultRequest.ocrRsltOrigItem = JSON.parse(resultRequest.ocrRsltOrigItem);
      }
      if (resultRequest.ocrRsltModItem) {
        resultRequest.ocrRsltModItem = JSON.parse(resultRequest.ocrRsltModItem);
      }

      database.models.ocrTmplMgmt.findOne({
        attributes: selectAttributesTemplate.concat(ocrCommonFields),
        where: {
          tmplUid: resultRequest.tmplUid
        }
      })
        .then((resultTmpl) => {
          resultTmpl.tmplOcrItem = JSON.parse(resultTmpl.tmplOcrItem);
          const resultSet = {
            tmplate: resultTmpl,
            result: resultRequest
          };

          const message = Message(0, resultSet);
          res.returnSuccess(message);
        })
        .catch((e) => {
          // console.error(e);
          res.returnServerError(e);
        });
    })
    .catch((e) => {
      res.returnServerError(e);
    });
});

// 요청 내역 상태 변경
app.post("/ocr/updateOcrRequestStatus", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;

  if (!reqItem || !reqItem.ocrRqstUid || !reqItem.ocrStat) {
    res.returnInvalidParameter();
    return;
  }

  database.models.ocrRqst.update(
    { ocrStat: reqItem.ocrStat },
    { where: { ocrRqstUid: reqItem.ocrRqstUid } }
  )
    .then((resultUpdate) => {
      const message = Message(0, resultUpdate);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// OCR 처리결과 수정
app.post("/ocr/updateOcrRequestResult", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;

  if (!reqItem || !reqItem.ocrRqstUid || !reqItem.ocrRsltModItem) {
    res.returnInvalidParameter();
    return;
  }

  database.models.ocrRqst.update(
    { ocrRsltModItem: reqItem.ocrRsltModItem },
    { where: { ocrRqstUid: reqItem.ocrRqstUid } }
  )
    .then((resultUpdate) => {
      const message = Message(0, resultUpdate);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

module.exports = app;
