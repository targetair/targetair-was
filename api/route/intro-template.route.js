const express = require("../network/express.js");

const app = express.Router();


app.post("/intro/analyze/ai", (req, res) => {
    console.log('/intro/analyzi/ai');

    let file     = req.files.file;


    let codeMapping = {
          idCard             : {typeName : '주민등록증'    , typeCode : '4'}
        , driversLicense     : {typeName : '운전면허증'    , typeCode : '2'}
        , passport           : {typeName : '주민등록발급'  , typeCode : '3'}
        , businessLicense    : {typeName : '사업자등록증'  , typeCode : '0'}
        , homeMembersCert    : {typeName : '주민등록등본'  , typeCode : '7'}
        , personalRegistCert : {typeName : '주민등록초본'  , typeCode : '6'}
        , familyCert         : {typeName : '가족관계증명서', typeCode : '1'}
        , armyService        : {typeName : '입영통지서'    , typeCode : '8'}
        , corpStampCert      : {typeName : '법인인감증명'  , typeCode : '5'}
        , uncategorized      : {typeName : '알수없음'      , typeCode : '10'}
    }

    let rtn     = {
          fileName : file.name
        , typeName : codeMapping.businessLicense.typeName
        , typeCode : codeMapping.businessLicense.typeCode
    }
    res.json(rtn);
    return;
});

app.post("/intro/analyze/ocr", (req, res) => {
    console.log('/intro/analyzi/ocr');

    var file     = req.files.file;

    let rtn     = [
          {no : '1', category : '사업자유형', contents : '법인사업자', accuracy : '100'}
        , {no : '2', category : '등록번호', contents : '117-81-65799', accuracy : '100'}
        , {no : '3', category : '법인명(단체명)', contents : '주식회사 케이티디에스', accuracy : '99'}
        , {no : '4', category : '대표자', contents : '우정민', accuracy : '100'}
    ];
    res.json(rtn);
    return;
});

module.exports = app;
