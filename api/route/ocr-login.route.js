// const moment = require("moment");
// const Sequelize = require("sequelize");
const express = require("../network/express.js");
// const database = require("../controller/database/database.js");
// const ocrCommonFields = require("../controller/database/ocr-common.database.js");
// const orchestratorFunctions = require("../controller/orchestrator/orchestrator.js");
// const upload = require("../controller/upload.js");

const Message = express.Message;
const app = express.Router();

// ocr 요청 리스트 조회 (getOcrRequestList)
app.post("/ocr/login", (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  console.log("reqItem", reqItem);
  // console.log("req", req);
  const returnMsg = {};

  if (reqItem.userId == "developer1") {
    returnMsg.userUid = "1000";
    returnMsg.userId = reqItem.userId;
    returnMsg.userNm = "개발자1";
    returnMsg.ariBrc = "B0001";
    returnMsg.ariBrnm = "기술팀";
  } else if (reqItem.userId == "developer2") {
    returnMsg.userUid = "2000";
    returnMsg.userId = reqItem.userId;
    returnMsg.userNm = "개발자2";
    returnMsg.ariBrc = "B0002";
    returnMsg.ariBrnm = "개발팀";
  } else {
    res.returnNotFound();
    return;
  }

  req.session.user = returnMsg;
  console.log("req.session", req.session);
  const message = Message(0, returnMsg);
  res.returnSuccess(message);
});

module.exports = app;
