// const moment = require("moment");
const Sequelize = require("sequelize");
const express = require("../network/express.js");
const database = require("../controller/database/database.js");
const ocrCommonFields = require("../controller/database/ocr-common.database.js");
// const orchestratorFunctions = require("../controller/orchestrator/orchestrator.js");
// const upload = require("../controller/upload.js");
const Op = Sequelize.Op;

const Message = express.Message;
const app = express.Router();

// 템플릿 제안 리스트 조회 (사용자)
app.get("/ocr/getTmplProposalList", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  // console.log("reqItem", reqItem);
  // console.log("req", req.session);

  const pagination = reqItem && reqItem.pagination ? reqItem.pagination : null;

  let searchParam = {};
  if (reqItem && reqItem.search) {
    const searchParse = JSON.parse(reqItem.search);
    searchParam = {
      searchText: searchParse.searchText ? searchParse.searchText || "" : "",
      searchStatus: searchParse.searchStatus ? searchParse.searchStatus || "" : "",
    };
  }

  const userUid = req.session.user.userUid;
  const paging = database.paginationToOptions(pagination);
  // console.log("paging", paging);

  const where = {};
  where[Op.and] = [{ userUid }];

  // where.userUid = userUid;
  if (searchParam.searchStatus) {
    where[Op.and].push({ ocrStat: searchParam.searchStatus });
  }
  if (searchParam.searchText) {
    where[Op.or] = [
      { sgstTmplNm: { [Op.like]: `%${searchParam.searchText}%` } },
      { userNm: { [Op.like]: `%${searchParam.searchText}%` } }
    ];
  }
  // console.log("where", where);

  const selectAttributes = ["tmplSgstUid", "sgstTmplNm", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "sgstStat", "rqstOcrItem", "rjctDesc"];
  database.models.ocrTmplSgst.findAndCountAll({
    attributes: selectAttributes.concat(ocrCommonFields),
    where,
    offset: paging.offset,
    limit: paging.limit,
    order: [["createdAt", "DESC"]]
  })
    .then((result) => {
      console.log(`getTmplProposalList result total ${result.count}, fetch count : ${result.rows.length}`);
      const message = Message(0, result.rows, result.count);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// 템플릿 제안 리스트 조회 (관리자)
app.get("/ocr/getTmplProposalListAdmin", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  // console.log("reqItem", reqItem);
  // console.log("req", req.session);

  const pagination = reqItem && reqItem.pagination ? reqItem.pagination : null;

  let searchParam = {};
  if (reqItem && reqItem.search) {
    const searchParse = JSON.parse(reqItem.search);
    searchParam = {
      searchText: searchParse.searchText ? searchParse.searchText || "" : "",
      searchStatus: searchParse.searchStatus ? searchParse.searchStatus || "" : "",
    };
  }

  const paging = database.paginationToOptions(pagination);
  // console.log("paging", paging);

  const where = {};
  // where.userUid = userUid;
  if (searchParam.searchStatus) {
    where[Op.and] = [{ sgstStat: searchParam.searchStatus }];
  }

  if (searchParam.searchText) {
    where[Op.or] = [
      { sgstTmplNm: { [Op.like]: `%${searchParam.searchText}%` } },
      { userNm: { [Op.like]: `%${searchParam.searchText}%` } }
    ];
  }
  // console.log("where", where);

  const selectAttributes = ["tmplSgstUid", "sgstTmplNm", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "sgstStat", "rqstOcrItem", "rjctDesc"];
  database.models.ocrTmplSgst.findAndCountAll({
    attributes: selectAttributes.concat(ocrCommonFields),
    where,
    offset: paging.offset,
    limit: paging.limit,
    order: [["createdAt", "DESC"]]
  })
    .then((result) => {
      console.log(`getTmplProposalList result total ${result.count}, fetch count : ${result.rows.length}`);
      const message = Message(0, result.rows, result.count);
      res.returnSuccess(message);
    })
    .catch((e) => {
      console.error(e);
      res.returnServerError(e);
    });
});

// 템플릿 신규제안 등록
app.post("/ocr/createTemplateProposal", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;
  const reqFiles = req.files;

  if (!reqFiles || !reqFiles.uploadFile) {
    res.returnInvalidAttachment();
    return;
  }

  const user = req.session.user;

  database.models.ocrTmplSgst.sequence()
    .then((result) => {
      console.log(`New sequence : ${result}`);
      const insertData = {
        tmplSgstUid: result,
        userUid: user.userUid,
        userNm: user.userNm,
        ariBrc: user.ariBrc,
        ariBrnm: user.ariBrnm,
        delYn: "N",
        sgstTmplNm: reqItem.sgstTmplNm,
        pupsUse: reqItem.pupsUse,
        rqstOcrItem: reqItem.rqstOcrItem,
        imgFileName: reqItem.pupsUse,
        imgFileType: reqItem.imgFileType,
        imgPageCnt: reqItem.imgPageCnt,
        imgOrgData: reqFiles.uploadFile.data.toString("base64"),
        sgstStat: "01"
      };

      database.models.ocrTmplSgst.create(insertData)
        .then((resultInsert) => {
          const message = Message(0, resultInsert);
          res.returnSuccess(message);
        })
        .catch((e) => {
          // console.error(e);
          res.returnServerError(e);
        });
    }).catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// 템플릿 제안 변경
app.post("/ocr/updateTemplateProposal", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;
  const reqFiles = req.files;

  if (!reqFiles || !reqFiles.uploadFile) {
    reqItem.imgOrgData = null;
  } else {
    reqItem.imgOrgData = reqFiles.uploadFile.data.toString("base64");
  }

  const updateData = {};

  if (reqItem.sgstTmplNm) updateData.sgstTmplNm = reqItem.sgstTmplNm;
  if (reqItem.pupsUse) updateData.pupsUse = reqItem.pupsUse;
  if (reqItem.rqstOcrItem) updateData.rqstOcrItem = reqItem.rqstOcrItem;
  if (reqItem.imgOrgData) {
    updateData.imgOrgData = reqItem.imgOrgData;
    updateData.imgFileName = reqItem.imgFileName;
    updateData.imgFileType = reqItem.imgFileType;
    updateData.imgPageCnt = reqItem.imgPageCnt;
  }

  database.models.ocrTmplSgst.update(
    updateData,
    { where: { tmplSgstUid: reqItem.tmplSgstUid } }
  )
    .then((resultUpdate) => {
      const message = Message(0, resultUpdate);
      res.returnSuccess(message);
    })
    .catch((e) => {
      // console.error(e);
      res.returnServerError(e);
    });
});

// OCR Template 제안 상세조회
app.get("/ocr/getTmplProposalDetail", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = Object.keys(req.query).length === 0 ? req.body : req.query;
  if (!reqItem || !reqItem.tmplSgstUid) {
    res.returnInvalidParameter();
    return;
  }
  const selectAttributes = ["tmplSgstUid", "sgstTmplNm", "pupsUse", "imgFileName", "imgFileType", "imgPageCnt", "imgOrgData", "sgstStat", "rqstOcrItem", "rjctDesc"];

  database.models.ocrTmplSgst.findOne({
    attributes: selectAttributes.concat(ocrCommonFields),
    where: { tmplSgstUid: reqItem.tmplSgstUid },
  })
    .then((resultRequest) => {
      const message = Message(0, resultRequest);
      res.returnSuccess(message);
    })
    .catch((e) => {
      res.returnServerError(e);
    });
});

// 템플릿 제안 상태 변경
app.post("/ocr/updateTemplateProposalStatus", express.isAuthenticated("test", "test"), (req, res) => {
  const reqItem = req.body;

  if (!reqItem || !reqItem.tmplSgstUid || !reqItem.sgstStat) {
    res.returnInvalidParameter();
    return;
  }

  const updateData = { sgstStat: reqItem.sgstStat };
  if (reqItem.rjctDesc) {
    updateData.rjctDesc = reqItem.rjctDesc;
  } else {
    updateData.rjctDesc = null;
  }

  database.models.ocrTmplSgst.update(
    updateData,
    { where: { tmplSgstUid: reqItem.tmplSgstUid } }
  )
    .then((resultUpdate) => {
      const message = Message(0, resultUpdate);
      res.returnSuccess(message);
    })
    .catch((e) => {
      console.error(e);
      res.returnServerError(e);
    });
});

module.exports = app;
