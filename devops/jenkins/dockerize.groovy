def label = "Jenkins-Slave-${UUID.randomUUID().toString()}"

String getBranchName(branch) {
    branchTemp=sh returnStdout:true ,script:"""echo "$branch" |sed -E "s#origin/##g" """
    if(branchTemp){
        branchTemp=branchTemp.trim()
    }
    return branchTemp
}


podTemplate(label: label, serviceAccount: 'jenkins-nh-rpa-ocr',
    containers: [
        containerTemplate(name: 'build-tools', image: 'registry.ktdscoe.myds.me:5500/alpine/build-tools:latest', ttyEnabled: true, command: 'cat', privileged: true, alwaysPullImage: true),
        containerTemplate(name: 'jnlp', image: 'registry.ktdscoe.myds.me:5500/jenkins/jnlp-slave:alpine', args: '${computer.jnlpmac} ${computer.name}')
    ],
    volumes: [
        hostPathVolume(hostPath: '/var/run/docker.sock', mountPath: '/var/run/docker.sock'),
        persistentVolumeClaim(mountPath: '/home/jenkins', claimName: 'jenkins-slave-pvc', readOnly: false)


        ]
    ) {
    node(label) {
        
        try {
            // freshStart 
            def freshStart = params.freshStart

            if ( freshStart ) {
                container('build-tools'){
                    // remove previous working dir
                    print "freshStart... clean working directory ${env.JOB_NAME}"
                    sh 'ls -A1|xargs rm -rf' /* clean up our workspace */
                }
            }

            
            def commitId

            def branchTemp
            //branch Name Parsing
            branchTemp = params.branchName
            branch=getBranchName(branchTemp)


            stage('Get Source') {
                git url: "https://gitlab.com/nh-rpa-ocr/nh-rpa-ocr-was.git",
                    credentialsId: 'gitlab-public-wsforyou-credential',
                    branch: "${branch}"
                    commitId = sh(script: 'git rev-parse --short HEAD', returnStdout: true).trim()
            }
            def props = readProperties  file:'devops/jenkins/dockerize.properties'
            def tag = commitId
            def dockerRegistry = props['dockerRegistry']
            def image = props['image']
            def selector = props['selector']
            def namespace = props['namespace']
            def appname = props['appname']


            stage('Build Docker image') {
                container('build-tools') {
                    docker.withRegistry("${dockerRegistry}", 'cluster-registry-credentials') {
                        sh "docker build -t ${image}:${tag} -f devops/jenkins/Dockerfile ."
                        sh "docker push ${image}:${tag}"
                        sh "docker tag ${image}:${tag} ${image}:latest"
                        sh "docker push ${image}:latest"
                    }
                }
            }

            stage( 'Helm lint' ) {
                container('build-tools') {
                    dir('devops/helm/nh-rpa-ocr-was'){
                        sh """
                        helm lint --namespace nh-rpa-ocr .
                        """
                    }
                }
            }


            stage("Summary") {
                print "Thank you."
            }

        } catch(e) {
            container('build-tools'){
                print "Clean up ${env.JOB_NAME} workspace..."
                sh 'ls -A1|xargs rm -rf' /* clean up our workspace */
            }

            print " **Error :: " + e.toString()+"**"
            currentBuild.result = "FAILED"
        }
    }
}
