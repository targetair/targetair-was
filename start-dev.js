console.log(`Start with ${process.env.NODE_ENV}`);
console.log(`Operating mode is ${process.env.RUN_MODE}`);

const debug = require("debug")("no-express-nh-rpa-ocr:server");
const http = require("http");
const apiApp = require("./api/api.app.js");
const database = require("./api/controller/database/database.js");

let server = null;
const port = "9081";

function onListening() {
  const addr = server.address();
  const bind = typeof addr === "string"
    ? `pipe ${addr}`
    : `port ${addr.port}`;
  debug(`Listening on ${bind}`);
}

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string"
    ? `Pipe ${port}`
    : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      console.error("error default", error);
      throw error;
  }
}

const connectionDB = () => {
  apiApp.confirmDB(database).then(() => {
    return apiApp.setup().then((app) => {
      console.log("db connect success.");
      // const config = require("./api/config/config.js");
      server = http.createServer(app);
      server.listen(port);
      server.on("error", onError);
      server.on("listening", onListening);
      console.log("listening success.");
      require("./api/daemon/rpa-image-daemon.js");
    });
  }).catch((err) => {
    console.error(err);

    // retry in 5sec
    setTimeout(() => {
      connectionDB();
    }, 5000);
  });
};

connectionDB();
