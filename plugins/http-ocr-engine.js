/* eslint-disable no-unused-vars */
const axios = require("axios");

axios.defaults.withCredentials = true;
axios.defaults.timeout = 120000; // 2min

// let host = process.env.BASE_URL || "";
const object = {};
// object.catch = (e) => {
//   const exceptionPath = ["/dev"];
//   const isExceptionPath = exceptionPath.some((path) => {
//     return path == window.location.pathname;
//   });
//   if (isExceptionPath) return Promise.reject("서버에 문제가 있습니다.");
//   if (e.response && e.response.status == 403) {
//     return Promise.reject("로그인이 필요합니다.");
//   } if (e.response && e.response.status == 500) {
//     return Promise.reject("서버에 문제가 있습니다.");
//   }
//   return Promise.reject("기타 오류가 있습니다.");
// };
// object.store = null;
// object.setExpiredAt = expiredAt => {
//   object.store.state.expiredAt = expiredAt;
// };

object.request = function (method, url, params = {}, retry = 0) {
  const options = {
    method,
    // url: host + url
    url
  };

  if (method === "post" || method === "put") {
    options.data = params;
  } else {
    options.params = params;
  }

  return axios(options)
    .then((res) => {
      // object.setExpiredAt(res.headers["expired-at"]);
      return Promise.resolve(res);
    })
    .catch((e) => {
      console.error(e.message);
      return Promise.reject("OCR 엔진 처리에 실패 하였 습니다.");
    });
};

// OCR Engine request
object.ocrEngineApiCall = (url, params) => {
  return object.request("post", url, params);
};

module.exports = object;
