/* eslint-disable no-await-in-loop */
require("dotenv").config();

const fs = require("fs");
const path = require("path");
const pdfPoppler = require("pdf-poppler");

const fsPromises = fs.promises;
const rpaDbServerice = require("./rpaDbService.js");

const pdfWorkPath = path.join(__dirname, "..", "..", "pdfwork");
// const testPdfFile = "/Users/bigfarm/MyData/WinShare/NH/PDF-Sample/file-sample_150kB_4p.pdf";
// const testPdfFile = "/Users/bigfarm/MyData/WinShare/NH/PDF-Sample/nh_ci.png";
// console.log("path object : ");
// console.log(path);
// console.log(`__dirname : ${__dirname}`);
console.log(`pdfWorkPath : ${pdfWorkPath}`);
// console.log(`pdf dirname : ${path.dirname(testPdfFile)}`);
// console.log(`pdf basename : ${path.basename(testPdfFile)}`);
// console.log(`pdf name : ${path.parse(testPdfFile).name}`);
// console.log(`pdf extname : ${path.extname(testPdfFile)}`);

const pdfPopplerOption = {
  format: "png", // FORMATS = ['png', 'jpeg', 'tiff', 'pdf', 'ps', 'eps', 'svg']
  out_dir: pdfWorkPath,
  out_prefix: "file",
  scale: 1024,
  page: null
};

const sleep = (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

async function clearJobDirectory() {
  try {
    if (fs.existsSync(pdfWorkPath)) {
      console.log("     Job directory clear start.");
      const files = await fsPromises.readdir(pdfWorkPath);
      const unlinkPromises = files.map((filename) => {
        console.log(`     unlink file : ${filename}`);
        return fsPromises.unlink(`${pdfWorkPath}/${filename}`);
      });

      await Promise.all(unlinkPromises);
      console.log("     Done.");
    } else {
      fs.mkdirSync(pdfWorkPath);
      console.log("     Job directory created.");
    }
  } catch (err) {
    console.error(err.message);
    throw err;
  }
}

async function convertPdfPoppler(pdfFile) {
  let result = null;

  try {
    pdfPopplerOption.out_prefix = path.parse(pdfFile).name;
    const start = new Date();

    let pdfinfo = null;
    await pdfPoppler.info(pdfFile)
      .then((pdfinfoResult) => {
        pdfinfo = pdfinfoResult;
      })
      .catch((error) => {
        throw error;
      });

    await pdfPoppler.convert(pdfFile, pdfPopplerOption)
      .then(() => {
        console.log("  => Successfully converted.");
      })
      .catch((error) => {
        throw error;
      });

    const end = new Date() - start;
    console.log("     PDF2IMG Covert execution time: %dms", end);
    result = {
      result: "success",
      message: []
    };
    const pdfPageSize = parseInt(pdfinfo.pages, 10);
    for (let idx = 0; idx < pdfPageSize; idx += 1) {
      const pageInfo = {
        page: idx + 1,
        name: `${pdfPopplerOption.out_prefix}-${idx + 1}.${pdfPopplerOption.format}`
      };
      result.message.push(pageInfo);
    }
    // console.log(result);
  } catch (err) {
    console.error(`[[${err}]]`);
    throw err;
  }
  return result;
}

async function fileSaveFromBuffer(imgFileName, base64String) {
  const fullFileName = pdfWorkPath + path.sep + imgFileName;

  try {
    const buf = Buffer.from(base64String, "base64");
    await fsPromises.writeFile(fullFileName, buf);
  } catch (err) {
    console.error(err.message);
    throw err;
  }

  return fullFileName;
}

async function ocrEngineCall() {
  const max = 1000;
  const min = 100;
  const randSec = Math.floor(Math.random() * (max - min)) + min;

  try {
    const start = new Date();
    await sleep(randSec);
    const end = new Date() - start;
    console.log("     OCR Engine call execution time: %dms", end);
  } catch (err) {
    console.error(err.message);
    throw err;
  }

  return {
    itemNo1: "1111",
    itemNo2: "12-12-12",
    itemNo3: "Home Mart"
  };
}

async function processNewRequest() {
  const x = true;

  try {
    await clearJobDirectory();
  } catch (err) {
    console.error(err);
    throw err;
  }

  // 미처리된 request를 1건 읽어서 처리한다.
  // 없으면 10초 대기후 재조회를 수행한다.

  while (x) {
    try {
      const record = await rpaDbServerice.getOcrNewRequestOne();
      try {
        if (!record || !record.ocrRqstId) {
          console.debug("No more new request. wait 10 sec.");
          await sleep(10000);
        } else {
          console.log(`>>> Request ocr processing start. Req ID : ${record.ocrRqstId} >>>`);
          // 진행중 업데이트
          await rpaDbServerice.updateOcrRequest({ ocrRqstId: record.ocrRqstId, modiUserId: "system", ocrStat: "02" });
          const templateDatail = await rpaDbServerice.getOcrTemplateDetail(record.ocrTmplId);

          console.log(`  => filType : ${record.imgFileType}, fileName : ${record.imgFileName}`);

          const resultSet = [];

          // pdf 경우 파일로 저장후 각 페이지를 jpg로 변환한다.
          if (record.imgFileType === "application/pdf") {
            // 작업 디렉토리 정리
            console.log("  => File job directory reset.");
            await clearJobDirectory();
            // 파일 저장
            console.log(`  => PDF File saving : ${record.imgFileName}`);
            const saveFileName = await fileSaveFromBuffer(record.imgFileName, record.imgOrgData);
            console.log(`  => PDF File saved : ${saveFileName}`);

            console.log("  => PDF File convert to image start.");
            // const result = await convertPdfImage(saveFileName);
            const result = await convertPdfPoppler(saveFileName);

            console.log("  => PDF File convert to image end.");

            if (result.result != "success") {
              throw new Error("Pdf to image covert failed.");
            }

            // ocr engine call start
            console.log("  => OCR Engine call start.");
            for (let idx = 0; idx < result.message.length; idx += 1) {
              const resultMap = JSON.parse(JSON.stringify(templateDatail.tmplOcrItem));
              console.log(`     page : ${result.message[idx].page} call start..`);
              // eslint-disable-next-line no-unused-vars
              const ocrCallResult = await ocrEngineCall();
              resultMap[0].itemData = idx.toString();
              resultMap.forEach((f) => { f.itemData = `Value for [${f.itemName}] of Page ${(idx + 1).toString()}`; });
              // resultSet.push(ocrCallResult);
              const resultPageMap = {
                pageNo: idx + 1,
                pageResultData: resultMap
              };
              resultSet.push(resultPageMap);
            }

            console.log("  => OCR Engine call end.");
            // ocr engine call end
          } else {
            // image 변환
            // ocr endgine call start.
            // call start ..................
            // console.log(templateDatail.tmplOcrItem);

            const resultMap = JSON.parse(JSON.stringify(templateDatail.tmplOcrItem));
            console.log("  => OCR Engine call start.");
            // eslint-disable-next-line no-unused-vars
            const ocrCallResult = await ocrEngineCall();
            console.log("  => OCR Engine call end.");
            // call..............
            // call end ...................
            // resultSet.push(ocrCallResult);
            resultMap.forEach((f) => { f.itemData = `Value for [${f.itemName}]`; });
            // resultMap[0].itemData = "ONE ITEM";
            const resultPageMap = {
              pageNo: 1,
              pageResultData: resultMap
            };
            resultSet.push(resultPageMap);
          }
          await rpaDbServerice.updateOcrRequest({
            ocrRqstId: record.ocrRqstId,
            ocrStat: "03",
            modiUserId: "system",
            ocrRsltOrigItem: JSON.stringify(resultSet),
            ocrRsltModiItem: JSON.stringify(resultSet)
          });
          console.log("  => Updated db status.");
          // console.log(JSON.stringify(resultSet, null, 4));
          console.log(`<<< Request ocr processing start. Req ID : ${record.ocrRqstId} <<<`);
        }
        // if (x) break;
      } catch (err) {
        await rpaDbServerice.updateOcrRequest({ ocrRqstId: record.ocrRqstId, modiUserId: "system", ocrStat: "04" });
        throw err;
      }
    } catch (err) {
      // do not throw.
      console.error(err.message);
      await sleep(10000);
    }
    // await sleep(10000);
  }
}

// clearJobDirectory();
// convertPdfPoppler(testPdfFile);
processNewRequest();

module.exports = {
};
