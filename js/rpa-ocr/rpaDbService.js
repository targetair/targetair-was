require("dotenv").config();

let {
  // eslint-disable-next-line prefer-const
  DB_SERVICE_HOST, DB_SERVICE_PORT, DB_ENV_DB, DB_USER, DB_PASSWORD
} = process.env;
DB_USER = DB_USER.trim().replace("\\n", "");
DB_PASSWORD = DB_PASSWORD.trim().replace("\\n", "");
// eslint-disable-next-line max-len
// console.log("for checking => ", DB_SERVICE_HOST, DB_SERVICE_PORT, DB_ENV_DB, DB_USER, DB_PASSWORD);

// mssql
const sql = require("mssql");
// eslint-disable-next-line no-unused-vars
const { types } = require("pg");

let connection = null;

const config = {
  server: DB_SERVICE_HOST,
  port: parseInt(DB_SERVICE_PORT, 10),
  user: DB_USER,
  password: DB_PASSWORD,
  database: DB_ENV_DB,
  requestTimeout: 30000,
  options: {
    enableArithAbort: true,
    useUTC: false
  }
};

async function sqlConnect() {
  try {
    connection = new sql.ConnectionPool(config);
    await connection.connect();
    console.log("Connection Successful !");
  } catch (err) {
    console.error("error----- ", err);
    throw new Error(`Error : ${err.message}`);
  }
}

async function sqlRequest(transaction) {
  try {
    if (!connection) await sqlConnect();
    if (connection) {
      return new sql.Request(transaction || connection);
    }

    throw new Error("Invalid Sql Connection.");
  } catch (err) {
    console.error("error----- ", err);
    throw new Error(`Error : ${err.message}`);
  }
}

async function sqlTransaction() {
  try {
    if (!connection) await sqlConnect();
    if (connection) {
      return new sql.Transaction(connection);
    }

    throw new Error("Invalid Sql Connection.");
  } catch (err) {
    console.error("error----- ", err);
    throw new Error(`Error : ${err.message}`);
  }
}

module.exports = {
  async getOcrRequestTotalCount(searchParam) {
    let selectSql = `
      SELECT COUNT(A.OCR_RQST_ID) AS totalCount
        FROM OCR_RQST A
        LEFT OUTER JOIN OCR_TMPL_MNGE B 
          ON A.OCR_TMPL_ID = B.OCR_TMPL_ID 
       WHERE 1 = 1
    `;

    if (searchParam && searchParam.searchText) {
      selectSql += `
          AND (A.PUPS_USE LIKE CONCAT('%', @searchText, '%') OR
               A.USER_NAME LIKE CONCAT('%', @searchText, '%') OR  
               B.TMPL_NAME LIKE CONCAT('%', @searchText, '%'))
      `;
    }

    if (searchParam && searchParam.searchStatus) {
      selectSql += `
          AND A.OCR_STAT = @searchStatus
      `;
    }

    if (searchParam && searchParam.userId) {
      selectSql += `
          AND A.USER_ID = @userId
      `;
    }

    const request = await sqlRequest();
    if (searchParam && searchParam.searchText) {
      request.input("searchText", searchParam.searchText);
    }
    if (searchParam && searchParam.searchStatus) {
      request.input("searchStatus", searchParam.searchStatus);
    }
    if (searchParam && searchParam.userId) {
      request.input("userId", searchParam.userId);
    }

    const recordset = await request.query(selectSql);

    if (recordset.recordset.length > 0) {
      if (recordset.recordset[0].totalCount) {
        return recordset.recordset[0].totalCount;
      }
    }

    return 0;
  },
  async getOcrRequestList(searchParam) {
    let selectSql = `
    SELECT A.OCR_RQST_ID AS ocrRqstId
          ,A.CRET_USER_ID    AS cretUserId
          ,A.CRET_DATE       AS cretDate
          ,A.MODI_USER_ID    AS modiUserId
          ,A.MODI_DATE       AS modiDate   
          ,A.LAST_API_NAME   AS lastApiName
          ,A.EXPR_DATE       AS exprDate
          ,A.OCR_TMPL_ID     AS ocrTmplId
          ,B.TMPL_NAME       AS tmplName
          ,A.IMG_FILE_NAME   AS imgFileName
          ,A.IMG_FILE_TYPE   AS imgFileType
          ,A.IMG_PAGE_CNT    AS imgPageCnt
          ,A.USER_NAME       AS userName
          ,A.ORGN_ID         AS orgnId
          ,A.ORGN_NAME       AS orgnName
          ,A.REGI_DATE       AS regiDate
          ,A.PUPS_USE        AS pupsUse
          ,A.OCR_STAT        AS ocrStat
          ,OCR_RSLT_MODI_ITEM AS ocrRsltModiItem
          ,A.CMPL_DATE       AS cmplDate
          ,A.RETRY_CNT       AS retryCnt 
      FROM OCR_RQST A
      LEFT OUTER JOIN OCR_TMPL_MNGE B 
        ON A.OCR_TMPL_ID = B.OCR_TMPL_ID 
     WHERE 1 = 1
    `;

    if (searchParam && searchParam.searchText) {
      selectSql += `
          AND (A.PUPS_USE LIKE CONCAT('%', @searchText, '%') OR
               A.USER_NAME LIKE CONCAT('%', @searchText, '%') OR 
               B.TMPL_NAME LIKE CONCAT('%', @searchText, '%'))
      `;
    }

    if (searchParam && searchParam.searchStatus) {
      selectSql += `
          AND A.OCR_STAT = @searchStatus
      `;
    }

    if (searchParam && searchParam.userId) {
      selectSql += `
          AND A.USER_ID = @userId
      `;
    }

    selectSql += `
      ORDER BY A.REGI_DATE DESC
      OFFSET @skipOffset ROWS
      FETCH NEXT @numPerPage ROWS ONLY
    `;

    const request = await sqlRequest();
    request.input("skipOffset", (searchParam.currentPage - 1) * searchParam.itemsPerPage);
    request.input("numPerPage", searchParam.itemsPerPage);
    if (searchParam && searchParam.searchText) {
      request.input("searchText", searchParam.searchText);
    }
    if (searchParam && searchParam.searchStatus) {
      request.input("searchStatus", searchParam.searchStatus);
    }
    if (searchParam && searchParam.userId) {
      request.input("userId", searchParam.userId);
    }
    // console.log(searchParam);
    const recordset = await request.query(selectSql);
    // console.dir(recordset);
    return recordset;
  },
  async getOcrRequestDetail(ocrRqstId) {
    const selectSql = `
      SELECT A.OCR_RQST_ID     AS ocrRqstId
            ,A.CRET_USER_ID    AS cretUserId
            ,A.CRET_DATE       AS cretDate
            ,A.MODI_USER_ID    AS modiUserId
            ,A.MODI_DATE       AS modiDate   
            ,A.LAST_API_NAME   AS lastApiName
            ,A.EXPR_DATE       AS exprDate
            ,A.OCR_TMPL_ID     AS ocrTmplId
            ,B.TMPL_NAME       AS tmplName
            ,A.IMG_FILE_NAME   AS imgFileName
            ,A.IMG_FILE_TYPE   AS imgFileType
            ,A.IMG_PAGE_CNT    AS imgPageCnt
            ,A.USER_NAME       AS userName
            ,A.ORGN_ID         AS orgnId
            ,A.ORGN_NAME       AS orgnName
            ,A.REGI_DATE       AS regiDate
            ,A.IMG_ORG_DATA    AS imgOrgData
            ,A.PUPS_USE        AS pupsUse
            ,A.OCR_STAT        AS ocrStat
            ,A.CMPL_DATE       AS cmplDate
            ,A.OCR_RSLT_ORIG_ITEM AS ocrRsltOrigItem
            ,A.OCR_RSLT_MODI_ITEM AS ocrRsltModiItem
            ,A.RETRY_CNT       AS retryCnt 
        FROM OCR_RQST A
        LEFT OUTER JOIN OCR_TMPL_MNGE B 
          ON A.OCR_TMPL_ID = B.OCR_TMPL_ID 
       WHERE A.OCR_RQST_ID = @ocrRqstId
    `;

    const request = await sqlRequest();

    const recordset = await request
      .input("ocrRqstId", ocrRqstId)
      .query(selectSql);

    if (recordset.recordset.length > 0) {
      if (recordset.recordset[0].ocrRsltOrigItem) {
        // eslint-disable-next-line max-len
        recordset.recordset[0].ocrRsltOrigItem = JSON.parse(recordset.recordset[0].ocrRsltOrigItem);
        // console.log(recordset.recordset[0].ocrRsltOrigItem.length);
      }
      if (recordset.recordset[0].ocrRsltModiItem) {
        recordset.recordset[0].ocrRsltModiItem = JSON.parse(recordset.recordset[0].ocrRsltModiItem);
      }
      return recordset.recordset[0];
    }

    // if empty result
    const e = new Error("요청 결과가 없습니다.");
    e.status = 404;
    throw e;
  },
  async createOcrRequest(item) {
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);
      await requestinsert
        .input("cretUserId", item.cretUserId)
        .input("lastApiName", item.lastApiName)
        .input("ocrTmplId", item.ocrTmplId)
        .input("imgFileName", item.imgFileName)
        .input("imgFileType", item.imgFileType)
        .input("imgPageCnt", item.imgPageCnt)
        .input("userId", item.userId)
        .input("userName", item.userName)
        .input("orgnId", item.orgnId)
        .input("orgnName", item.orgnName)
        .input("imgOrgData", item.imgOrgData)
        .input("pupsUse", item.pupsUse)
        .input("ocrStat", item.ocrStat)
        .input("retryCnt", item.retryCnt)
        .query(`
          INSERT INTO dbo.OCR_RQST (
            CRET_USER_ID, 
            CRET_DATE, 
            LAST_API_NAME,
            OCR_TMPL_ID, 
            IMG_FILE_NAME, 
            IMG_FILE_TYPE, 
            IMG_PAGE_CNT, 
            USER_ID, 
            USER_NAME, 
            ORGN_ID, 
            ORGN_NAME, 
            REGI_DATE, 
            IMG_ORG_DATA, 
            PUPS_USE, 
            OCR_STAT,
            RETRY_CNT
          )
          VALUES (
            @cretUserId, 
            getdate(), 
            @lastApiName,
            @ocrTmplId, 
            @imgFileName, 
            @imgFileType,
            @imgPageCnt, 
            @userId, 
            @userName, 
            @orgnId, 
            @orgnName, 
            getdate(), 
            @imgOrgData, 
            @pupsUse, 
            @ocrStat, 
            @retryCnt
          )
        `);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async getOcrNewRequestOne() {
    const selectSql = `
      SELECT TOP 1
             OCR_RQST_ID     AS ocrRqstId
            ,OCR_TMPL_ID     AS ocrTmplId
            ,IMG_FILE_NAME   AS imgFileName
            ,IMG_FILE_TYPE   AS imgFileType
            ,IMG_PAGE_CNT    AS imgPageCnt
            ,REGI_DATE       AS regiDate
            ,IMG_ORG_DATA    AS imgOrgData
            ,OCR_STAT        AS ocrStat
        FROM OCR_RQST
       WHERE (
              OCR_STAT IN ('01', '05') OR
              (OCR_STAT = '02' AND MODI_DATE < DateADD(mi, -60, Current_TimeStamp))
             )
    `;

    const request = await sqlRequest();

    const recordset = await request.query(selectSql);

    if (recordset.recordset.length > 0) {
      return recordset.recordset[0];
    }

    return null;
  },
  async updateOcrRequest(item) {
    if (!item || !item.ocrRqstId) {
      throw new Error("updateOcrRequest : parameter is missing.");
    }

    let transaction = null;

    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);

      await requestinsert.input("ocrRqstId", item.ocrRqstId);
      if (item.ocrStat) requestinsert.input("ocrStat", item.ocrStat);
      if (item.ocrRsltOrigItem) requestinsert.input("ocrRsltOrigItem", item.ocrRsltOrigItem);
      if (item.ocrRsltModiItem) requestinsert.input("ocrRsltModiItem", item.ocrRsltModiItem);
      if (item.modiUserId) requestinsert.input("modiUserId", item.modiUserId);

      let sqlText = "UPDATE OCR_RQST\n   SET MODI_DATE = getdate()\n";
      if (item.ocrStat) {
        sqlText += "      ,OCR_STAT = @ocrStat\n";
        if (item.ocrStat === "03") {
          sqlText += "      ,CMPL_DATE = getdate()\n";
        }
      }
      sqlText += item.ocrRsltOrigItem ? "     ,OCR_RSLT_ORIG_ITEM = @ocrRsltOrigItem\n" : "";
      sqlText += item.ocrRsltModiItem ? "     ,OCR_RSLT_MODI_ITEM = @ocrRsltModiItem\n" : "";
      sqlText += item.modiUserId ? "     ,MODI_USER_ID = @modiUserId\n" : "";
      sqlText += " WHERE OCR_RQST_ID = @ocrRqstId";

      await requestinsert.query(sqlText);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async getOcrRequestStatistics(searchParam) {
    let selectSql = "";
    if (searchParam && searchParam.userId) {
      selectSql = `
        SELECT 'accumulate' id, COUNT(1) number
          FROM dbo.OCR_RQST
         WHERE USER_ID = @userId
        UNION ALL
        SELECT 'today' id, COUNT(1) number
          FROM dbo.OCR_RQST
        WHERE REGI_DATE  >= CAST(GETDATE() AS DATE)
          AND USER_ID = @userId
        UNION ALL
        SELECT 'processing' id, COUNT(1) number
          FROM dbo.OCR_RQST
        WHERE OCR_STAT IN ('01','02')
          AND USER_ID = @userId
        UNION ALL
        SELECT 'error' id, COUNT(1) number
          FROM dbo.OCR_RQST
        WHERE OCR_STAT = '04'
          AND USER_ID = @userId
      `;
    } else {
      selectSql = `
        SELECT 'accumulate' id, COUNT(1) number
          FROM dbo.OCR_RQST
        UNION ALL
        SELECT 'today' id, COUNT(1) number
          FROM dbo.OCR_RQST
        WHERE REGI_DATE  >= CAST(GETDATE() AS DATE)
        UNION ALL
        SELECT 'processing' id, COUNT(1) number
          FROM dbo.OCR_RQST
        WHERE OCR_STAT IN ('01','02')
        UNION ALL
        SELECT 'error' id, COUNT(1) number
          FROM dbo.OCR_RQST
        WHERE OCR_STAT = '04'
      `;
    }

    try {
      const request = await sqlRequest();

      if (searchParam && searchParam.userId) {
        request.input("userId", searchParam.userId);
      }

      const recordset = await request.query(selectSql);

      return recordset;
    } catch (err) {
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async getOcrTemplateList() {
    const selectSql = `
    SELECT OCR_TMPL_ID       AS ocrTmplId
          , CRET_USER_ID      AS cretUserId
          , CRET_DATE         AS cretDate
          , MODI_USER_ID      AS modiUserId
          , MODI_DATE         AS modiDate
          , EXPR_DATE         AS exprDate
          , TMPL_NAME         AS tmplName
          , TMPL_DESC         AS tmplDesc
          , TMPL_USE_STAT     AS tmplUseStat
          , TMPL_OCR_ITEM_CNT AS tmplOcrItemCnt
          , TMPL_OCR_ITEM     AS tmplOcrItem
          , TMPL_IMG_TYPE     AS tmplImgType
          , TMPL_EFCT_DATE    AS tmplEfctDate
          , TMPL_EXPR_DATE    AS tmplExprDate
          , OCR_API_URL       AS ocrApiUrl
      FROM OCR_TMPL_MNGE
      ORDER BY OCR_TMPL_ID DESC
      `;

    const request = await sqlRequest();
    const recordset = await request.query(selectSql);

    if (recordset.recordset.length > 0) {
      for (let idx = 0; idx < recordset.recordset.length; idx += 1) {
        recordset.recordset[idx].tmplOcrItem = JSON.parse(recordset.recordset[idx].tmplOcrItem);
      }
    }

    return recordset;
  },
  async getOcrTemplateAvailableList() {
    const selectSql = `
    SELECT OCR_TMPL_ID       AS ocrTmplId
          -- , CRET_USER_ID      AS cretUserId
          -- , CRET_DATE         AS cretDate
          -- , MODI_USER_ID      AS modiUserId
          -- , MODI_DATE         AS modiDate
          -- , EXPR_DATE         AS exprDate
          , TMPL_NAME         AS tmplName
          , TMPL_DESC         AS tmplDesc
          , TMPL_USE_STAT     AS tmplUseStat
          -- , TMPL_OCR_ITEM_CNT AS tmplOcrItemCnt
          -- , TMPL_OCR_ITEM     AS tmplOcrItem
          -- , TMPL_IMG_TYPE     AS tmplImgType
          -- , TMPL_EFCT_DATE    AS tmplEfctDate
          -- , TMPL_EXPR_DATE    AS tmplExprDate
          -- , OCR_API_URL       AS ocrApiUrl
      FROM OCR_TMPL_MNGE
     WHERE TMPL_USE_STAT = '02'
      ORDER BY OCR_TMPL_ID
      `;

    const request = await sqlRequest();
    const recordset = await request.query(selectSql);
    return recordset;
  },
  async getOcrTemplateDetail(ocrTmplId) {
    const selectSql = `
       SELECT OCR_TMPL_ID       AS ocrTmplId
            , CRET_USER_ID      AS cretUserId
            , CRET_DATE         AS cretDate
            , MODI_USER_ID      AS modiUserId
            , MODI_DATE         AS modiDate
            , EXPR_DATE         AS exprDate
            , TMPL_NAME         AS tmplName
            , TMPL_DESC         AS tmplDesc
            , TMPL_USE_STAT     AS tmplUseStat
            , TMPL_OCR_ITEM_CNT AS tmplOcrItemCnt
            , TMPL_OCR_ITEM     AS tmplOcrItem
            , TMPL_IMG_TYPE     AS tmplImgType
            , TMPL_IMG_DATA     AS tmplImgData
            , TMPL_EFCT_DATE    AS tmplEfctDate
            , TMPL_EXPR_DATE    AS tmplExprDate
            , OCR_API_URL       AS ocrApiUrl
         FROM OCR_TMPL_MNGE
        WHERE OCR_TMPL_ID = @ocrTmplId
      `;

    const request = await sqlRequest();

    const recordset = await request
      .input("ocrTmplId", ocrTmplId)
      .query(selectSql);

    if (recordset.recordset.length > 0) {
      if (recordset.recordset[0].tmplOcrItem) {
        recordset.recordset[0].tmplOcrItem = JSON.parse(recordset.recordset[0].tmplOcrItem);
      }
      return recordset.recordset[0];
    }

    // if empty result
    const e = new Error("요청 결과가 없습니다.");
    e.status = 404;
    throw e;
  },
  async createOcrTemplate(item) {
    // console.log(item);
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);
      await requestinsert
        .input("cretUserId", item.cretUserId)
        .input("lastApiName", item.lastApiName)
        .input("tmplName", item.tmplName)
        .input("tmplDesc", item.tmplDesc)
        .input("tmplImgType", item.tmplImgType)
        .input("tmplImgData", item.tmplImgData)
        .input("tmplOcrItem", item.tmplOcrItem)
        .input("tmplOcrItemCnt", item.tmplOcrItemCnt)
        .input("ocrApiUrl", item.ocrApiUrl)
        .input("tmplUseStat", item.tmplUseStat)
        .query(`
          INSERT INTO dbo.OCR_TMPL_MNGE (
            CRET_USER_ID, 
            CRET_DATE, 
            LAST_API_NAME,
            TMPL_NAME, 
            TMPL_DESC,
            TMPL_IMG_TYPE, 
            TMPL_IMG_DATA, 
            TMPL_OCR_ITEM,
            TMPL_OCR_ITEM_CNT, 
            OCR_API_URL,
            TMPL_USE_STAT
          )
          VALUES (
            @cretUserId, 
            getdate(), 
            @lastApiName,
            @tmplName, 
            @tmplDesc, 
            @tmplImgType, 
            @tmplImgData,
            @tmplOcrItem, 
            @tmplOcrItemCnt, 
            @ocrApiUrl,
            @tmplUseStat
          )
        `);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async updateOcrTemplate(item) {
    // console.log(item);
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);

      requestinsert.input("ocrTmplId", item.ocrTmplId);
      requestinsert.input("modiUserId", item.modiUserId);

      if (item.tmplName) requestinsert.input("tmplName", item.tmplName);
      if (item.tmplDesc) requestinsert.input("tmplDesc", item.tmplDesc);
      if (item.tmplOcrItem) requestinsert.input("tmplOcrItem", item.tmplOcrItem);
      if (item.tmplOcrItemCnt) requestinsert.input("tmplOcrItemCnt", item.tmplOcrItemCnt);
      if (item.ocrApiUrl) requestinsert.input("ocrApiUrl", item.ocrApiUrl);

      if (item.tmplImgData) {
        requestinsert.input("tmplImgData", item.tmplImgData);
        requestinsert.input("tmplImgType", item.tmplImgType);
      }

      let sqlText = "UPDATE OCR_TMPL_MNGE\n   SET MODI_DATE = getdate()\n";
      sqlText += "      ,MODI_USER_ID = @modiUserId\n";

      sqlText += item.tmplName ? "     ,TMPL_NAME = @tmplName\n" : "";
      sqlText += item.tmplDesc ? "     ,TMPL_DESC = @tmplDesc\n" : "";

      sqlText += item.tmplOcrItem ? "     ,TMPL_OCR_ITEM = @tmplOcrItem\n" : "";
      sqlText += item.tmplOcrItemCnt ? "     ,TMPL_OCR_ITEM_CNT = @tmplOcrItemCnt\n" : "";
      sqlText += item.ocrApiUrl ? "     ,OCR_API_URL = @ocrApiUrl\n" : "";

      if (item.tmplImgData) {
        sqlText += "     ,TMPL_IMG_DATA = @tmplImgData\n";
        sqlText += "     ,TMPL_IMG_TYPE = @tmplImgType\n";
      }
      sqlText += " WHERE OCR_TMPL_ID = @ocrTmplId";

      // console.log(`sqlText---\n${sqlText}`);

      await requestinsert.query(sqlText);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async updateOcrTemplateStatus(item) {
    // console.log(item);
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);

      requestinsert.input("ocrTmplId", item.ocrTmplId);
      requestinsert.input("modiUserId", item.modiUserId);
      requestinsert.input("tmplUseStat", item.tmplUseStat);

      let sqlText = "UPDATE OCR_TMPL_MNGE\n   SET MODI_DATE = getdate()\n";
      sqlText += "      ,MODI_USER_ID = @modiUserId\n";
      sqlText += "      ,TMPL_USE_STAT = @tmplUseStat\n";

      if (item.tmplUseStat == "02") {
        sqlText += "      ,TMPL_EFCT_DATE = getdate()\n";
        sqlText += "      ,TMPL_EXPR_DATE = NULL \n";
      } else if (item.tmplUseStat == "03") {
        sqlText += "      ,TMPL_EXPR_DATE = getdate() \n";
      }
      sqlText += " WHERE OCR_TMPL_ID = @ocrTmplId";
      // console.log(`sqlText---\n${sqlText}`);
      await requestinsert.query(sqlText);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async getTmplProposalTotalCount(searchParam) {
    let selectSql = `
    SELECT COUNT(A.OCR_SGST_ID) AS totalCount
      FROM OCR_SGST A
     WHERE 1 = 1
    `;

    if (searchParam && searchParam.searchText) {
      selectSql += `
          AND ( A.RQST_TMPL_NAME LIKE CONCAT('%', @searchText, '%') OR
                A.USER_NAME LIKE CONCAT('%', @searchText, '%') )
      `;
    }

    if (searchParam && searchParam.searchStatus) {
      selectSql += `
          AND A.SGST_STAT = @searchStatus
      `;
    }

    if (searchParam && searchParam.userId) {
      selectSql += `
          AND A.USER_ID = @userId
      `;
    }

    const request = await sqlRequest();

    if (searchParam && searchParam.searchText) {
      request.input("searchText", searchParam.searchText);
    }

    if (searchParam && searchParam.searchStatus) {
      request.input("searchStatus", searchParam.searchStatus);
    }

    if (searchParam && searchParam.userId) {
      request.input("userId", searchParam.userId);
    }

    const recordset = await request.query(selectSql);

    if (recordset.recordset.length > 0) {
      if (recordset.recordset[0].totalCount) {
        return recordset.recordset[0].totalCount;
      }
    }

    return 0;
  },
  async getTmplProposalList(searchParam) {
    let selectSql = `
      SELECT
              OCR_SGST_ID    AS ocrSgstId
            , CRET_USER_ID   AS cretUserId
            , CRET_DATE      AS cretDate
            , MODI_USER_ID   AS modiUserId
            , MODI_DATE      AS modiDate
            , LAST_API_NAME  AS lastApiName
            , EXPR_DATE      AS exprDate
            , RQST_TMPL_NAME AS rqstTmplName
            , RQST_USE       AS rqstUse
            , IMG_FILE_NAME  AS imgFileName
            , IMG_FILE_TYPE  AS imgFileType
            , IMG_PAGE_CNT   AS imgPageCnt
            , USER_ID        AS userId
            , USER_NAME      AS userName
            , ORGN_ID        AS orgnId
            , ORGN_NAME      AS orgnName
            , REGI_DATE      AS regiDate
            , RQST_OCR_ITEM  AS rqstOcrItem
            , SGST_STAT      AS sgstStat
            , OCR_TMPL_ID    AS ocrTmplId
            , CMPL_DATE      AS cmplDate
            , RSLT_OCR_ITEM  AS rsltOcrItem
            , RJCT_DESC      AS rjctDesc
         FROM OCR_SGST A
        WHERE 1 = 1 
    `;

    if (searchParam && searchParam.searchText) {
      selectSql += `
            AND ( A.RQST_TMPL_NAME LIKE CONCAT('%', @searchText, '%') OR
                  A.USER_NAME LIKE CONCAT('%', @searchText, '%') )
        `;
    }

    if (searchParam && searchParam.searchStatus) {
      selectSql += `
          AND A.SGST_STAT = @searchStatus
      `;
    }

    if (searchParam && searchParam.userId) {
      selectSql += `
          AND A.USER_ID = @userId
      `;
    }

    selectSql += `
      ORDER BY REGI_DATE DESC
      OFFSET @skipOffset ROWS
      FETCH NEXT @numPerPage ROWS ONLY
      `;

    const request = await sqlRequest();
    request.input("skipOffset", (searchParam.currentPage - 1) * searchParam.itemsPerPage);
    request.input("numPerPage", searchParam.itemsPerPage);
    if (searchParam && searchParam.searchText) {
      request.input("searchText", searchParam.searchText);
    }
    if (searchParam && searchParam.searchStatus) {
      request.input("searchStatus", searchParam.searchStatus);
    }
    if (searchParam && searchParam.userId) {
      request.input("userId", searchParam.userId);
    }

    const recordset = await request.query(selectSql);
    // console.dir(recordset);
    return recordset;
  },
  async getTmplProposalDetail(ocrSgstId) {
    const selectSql = `
    SELECT
            OCR_SGST_ID    AS ocrSgstId
          , CRET_USER_ID   AS cretUserId
          , CRET_DATE      AS cretDate
          , MODI_USER_ID   AS modiUserId
          , MODI_DATE      AS modiDate
          , LAST_API_NAME  AS lastApiName
          , EXPR_DATE      AS exprDate
          , RQST_TMPL_NAME AS rqstTmplName
          , RQST_USE       AS rqstUse
          , IMG_FILE_NAME  AS imgFileName
          , IMG_FILE_TYPE  AS imgFileType
          , IMG_PAGE_CNT   AS imgPageCnt
          , IMG_ORG_DATA   AS imgOrgData
          , USER_ID        AS userId
          , USER_NAME      AS userName
          , ORGN_ID        AS orgnId
          , ORGN_NAME      AS orgnName
          , REGI_DATE      AS regiDate
          , RQST_OCR_ITEM  AS rqstOcrItem
          , SGST_STAT      AS sgstStat
          , OCR_TMPL_ID    AS ocrTmplId
          , CMPL_DATE      AS cmplDate
          , RSLT_OCR_ITEM  AS rsltOcrItem
          , RJCT_DESC      AS rjctDesc
       FROM OCR_SGST
      WHERE OCR_SGST_ID = @ocrSgstId
    `;

    const request = await sqlRequest();

    const recordset = await request
      .input("ocrSgstId", ocrSgstId)
      .query(selectSql);

    if (recordset.recordset.length > 0) {
      return recordset.recordset[0];
    }

    // if empty result
    const e = new Error("요청 결과가 없습니다.");
    e.status = 404;
    throw e;
  },
  async createTemplateProposal(item) {
    // console.log(item);
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);
      await requestinsert
        .input("cretUserId", item.cretUserId)
        .input("lastApiName", item.lastApiName)
        .input("rqstTmplName", item.rqstTmplName)
        .input("rqstUse", item.rqstUse)
        .input("imgFileName", item.imgFileName)
        .input("imgFileType", item.imgFileType)
        .input("imgPageCnt", item.imgPageCnt)
        .input("userId", item.userId)
        .input("userName", item.userName)
        .input("orgnId", item.orgnId)
        .input("orgnName", item.orgnName)
        .input("imgOrgData", item.imgOrgData)
        .input("sgstStat", item.sgstStat)
        .query(`
          INSERT INTO dbo.OCR_SGST (
            CRET_USER_ID, 
            CRET_DATE, 
            LAST_API_NAME,
            RQST_TMPL_NAME, 
            RQST_USE,
            IMG_FILE_NAME, 
            IMG_FILE_TYPE, 
            IMG_PAGE_CNT, 
            USER_ID, 
            USER_NAME, 
            ORGN_ID, 
            ORGN_NAME, 
            REGI_DATE, 
            IMG_ORG_DATA, 
            SGST_STAT
          )
          VALUES (
            @cretUserId, 
            getdate(), 
            @lastApiName,
            @rqstTmplName, 
            @rqstUse, 
            @imgFileName, 
            @imgFileType,
            @imgPageCnt, 
            @userId, 
            @userName, 
            @orgnId, 
            @orgnName, 
            getdate(), 
            @imgOrgData, 
            @sgstStat
          )
        `);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async updateTemplateProposal(item) {
    // console.log(item);
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);

      requestinsert.input("ocrSgstId", item.ocrSgstId);
      requestinsert.input("modiUserId", item.modiUserId);

      if (item.rqstTmplName) requestinsert.input("rqstTmplName", item.rqstTmplName);
      if (item.rqstUse) requestinsert.input("rqstUse", item.rqstUse);
      if (item.imgOrgData) {
        requestinsert.input("imgOrgData", item.imgOrgData);
        requestinsert.input("imgFileName", item.imgFileName);
        requestinsert.input("imgFileType", item.imgFileType);
        requestinsert.input("imgPageCnt", item.imgPageCnt);
      }

      let sqlText = "UPDATE OCR_SGST\n   SET MODI_DATE = getdate()\n";
      sqlText += "      ,MODI_USER_ID = @modiUserId\n";

      sqlText += item.rqstTmplName ? "     ,RQST_TMPL_NAME = @rqstTmplName\n" : "";
      sqlText += item.rqstUse ? "     ,RQST_USE = @rqstUse\n" : "";
      if (item.imgOrgData) {
        sqlText += "     ,IMG_ORG_DATA = @imgOrgData\n";
        sqlText += "     ,IMG_FILE_NAME = @imgFileName\n";
        sqlText += "     ,IMG_FILE_TYPE = @imgFileType\n";
        sqlText += "     ,IMG_PAGE_CNT = @imgPageCnt\n";
      }
      sqlText += " WHERE OCR_SGST_ID = @ocrSgstId";

      // console.log(`sqlText---\n${sqlText}`);

      await requestinsert.query(sqlText);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  },
  async updateTemplateProposalStatus(item) {
    // console.log(item);
    let transaction = null;
    try {
      transaction = await sqlTransaction();
      await transaction.begin();
      const requestinsert = await sqlRequest(transaction);

      requestinsert.input("ocrSgstId", item.ocrSgstId);
      requestinsert.input("modiUserId", item.modiUserId);
      requestinsert.input("sgstStat", item.sgstStat);
      requestinsert.input("rjctDesc", item.rjctDesc);

      let sqlText = "UPDATE OCR_SGST\n   SET MODI_DATE = getdate()\n";
      sqlText += "      ,MODI_USER_ID = @modiUserId\n";
      sqlText += "      ,SGST_STAT = @sgstStat\n";
      sqlText += "      ,RJCT_DESC = @rjctDesc\n";
      if (item.sgstStat === "03") {
        sqlText += "     ,CMPL_DATE = getdate()\n";
      }
      sqlText += " WHERE OCR_SGST_ID = @ocrSgstId";
      // console.log(`sqlText---\n${sqlText}`);
      await requestinsert.query(sqlText);
      await transaction.commit();
    } catch (err) {
      if (transaction) await transaction.rollback();
      console.error("error----- ", err);
      throw new Error(`Error : ${err.message}`);
    }
  }
};
