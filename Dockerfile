FROM node
# 앱 디렉터리 생성
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# ENV PORT=3000
# ENV DB_TYPE=sqlite
# ENV DB_SERVICE_HOST=localhost
# ENV DB_SERVICE_PORT=5432
# ENV DB_ENV_DB=employees
# ENV DB_USER=admin
# ENV DB_PASSWORD=new1234@

# 앱 의존성 설치
# 가능한 경우(npm@5+) package.json과 package-lock.json을 모두 복사하기 위해
# 와일드카드를 사용
COPY . .

RUN npm install
# 프로덕션을 위한 코드를 빌드하는 경우
# RUN npm ci --only=production

# 앱 소스 추가

EXPOSE 3000
CMD [ "npm", "run", "start-dev" ]